<?php

namespace App\Http\Controllers\Admin\Bill;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Bill\Invoice;
use App\Model\Bill\InvoiceAsset;
use App\User;
use App\Model\Project\Order;
use App\Model\Project\Project;
use App\Model\Project\Service;
use App\Model\Setting\Setting;
use Session;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::all();
        return view('admin.bill.index', compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // client roll-Id is 2
        $clients = User::withTrashed()->where('role_id', 2)->get();
        $projects = Project::withTrashed()->get();
        $services = Service::withTrashed()->get(); //change to get

        return view('admin.bill.create', compact('clients', 'projects', 'services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                            'invoicefor' => 'required',
                            'gst'        => 'required',
                            'service'    => 'required',
                            'quantity'   => 'required',
                            'price'      => 'required',
                           ]);
        /*validation on project_id and order_id*/
        if($request->invoicefor == 'project'){
            $request->validate(['order_id' => 'required']);
        }
        elseif($request->invoicefor == 'client')
        {
             $request->validate(['client_id' => 'required']);
        }

        $last_invoice = Invoice::latest()->first();
   
        if(is_null($last_invoice)){
            $last_id = 1;
        }
        else{
            $last_id = $last_invoice->id+1;
        }


        $store_invoice = Invoice::create([
                                            'invoice_no'   => 'TST00'.$last_id,
                                            'client_id'    => $request->client_id,
                                            'order_id'     => $request->order_id,
                                            'gst'          => $request->gst,
                                         ]);
        $len = count($request->service);
        for ($x = 0; $x <$len; $x++){
            $store_invoice_asserts = InvoiceAsset::create([
                                                            'invoice_id' =>  $store_invoice->id,
                                                            'service_id' =>  $request->service[$x],
                                                            'quantity'   =>  $request->quantity[$x],
                                                            'price'      =>  $request->price[$x],
                                                          ]);
        }

        Session::flash('success', 'Invoice Created Successfully');
        return redirect()->route('bill.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Invoice::where('id',$id)->first();
        $tt_setting = Setting::first();
        // dd($data->order->project->user);
        return view('admin.bill.invoice', compact('data', 'tt_setting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_order($id){
        $data = Order::where('project_id', $id)->get()->pluck('order_id', 'id');
        return Response()->json($data);
    }
}
