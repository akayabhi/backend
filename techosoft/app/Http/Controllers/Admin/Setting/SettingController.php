<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Setting\Setting;
use Session;


class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = Setting::first();
        // dd($setting);
        return view('admin.setting.index', compact('setting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        if(is_null($request->logo)){
            $insert = Setting::create([
                                        "company_name"  => $request->company_name,
                                        "profession"    => $request->profession,
                                        "website_link"  => $request->website_link,
                                        "first_email"   => $request->first_email,
                                        "second_email"  => $request->second_email,
                                        "first_mobile"  => $request->first_mobile,
                                        "second_mobile" => $request->second_mobile,
                                        "address"       => $request->address,
                                        "bank_name"     => $request->bank_name,
                                        "account_no"    => $request->company_name,
                                        "ifsc"          => $request->ifsc,
                                        "upi_account"   => $request->upi_account,
                                        "gst"           => $request->gst,
                                        "founded"       => $request->founded,
                                        "bio"           => $request->bio,
                                      ]);    

        }
        else{
            $logo = $request->logo;
            $logo_name  =strtotime(date('Y-m-d H:i:s')).'_'.$logo->getClientOriginalName();
            $logo->move('img/setting', $logo_name);
            $insert = Setting::create([
                                        "logo"          => $logo_name,
                                        "company_name"  => $request->company_name,
                                        "profession"    => $request->profession,
                                        "website_link"  => $request->website_link,
                                        "first_email"   => $request->first_email,
                                        "second_email"  => $request->second_email,
                                        "first_mobile"  => $request->first_mobile,
                                        "second_mobile" => $request->second_mobile,
                                        "address"       => $request->address,
                                        "bank_name"     => $request->bank_name,
                                        "account_no"    => $request->company_name,
                                        "ifsc"          => $request->ifsc,
                                        "upi_account"   => $request->upi_account,
                                        "gst"           => $request->gst,
                                        "founded"       => $request->founded,
                                        "bio"           => $request->bio,
                                      ]);    

        }
        Session::flash('success', 'Company Profile Created Successfully');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if(is_null($request->logo)){
            $insert = Setting::where('id', $id)->update([
                                        "company_name"  => $request->company_name,
                                        "profession"    => $request->profession,
                                        "website_link"  => $request->website_link,
                                        "first_email"   => $request->first_email,
                                        "second_email"  => $request->second_email,
                                        "first_mobile"  => $request->first_mobile,
                                        "second_mobile" => $request->second_mobile,
                                        "address"       => $request->address,
                                        "bank_name"     => $request->bank_name,
                                        "account_no"    => $request->company_name,
                                        "ifsc"          => $request->ifsc,
                                        "upi_account"   => $request->upi_account,
                                        "gst"           => $request->gst,
                                        "founded"       => $request->founded,
                                        "bio"           => $request->bio,
                                      ]);    

        }
        else{
            $logo = $request->logo;
            $logo_name  =strtotime(date('Y-m-d H:i:s')).'_'.$logo->getClientOriginalName();
            $logo->move('img/setting', $logo_name);

            $check = Setting::where('id', $id)->first();
            $image_path = 'img/setting/'.$check->logo;
            if(file_exists($image_path)){
              unlink($image_path);
            }
            $insert = Setting::where('id', $id)->update([
                                        "logo"          => $logo_name,
                                        "company_name"  => $request->company_name,
                                        "profession"    => $request->profession,
                                        "website_link"  => $request->website_link,
                                        "first_email"   => $request->first_email,
                                        "second_email"  => $request->second_email,
                                        "first_mobile"  => $request->first_mobile,
                                        "second_mobile" => $request->second_mobile,
                                        "address"       => $request->address,
                                        "bank_name"     => $request->bank_name,
                                        "account_no"    => $request->company_name,
                                        "ifsc"          => $request->ifsc,
                                        "upi_account"   => $request->upi_account,
                                        "gst"           => $request->gst,
                                        "founded"       => $request->founded,
                                        "bio"           => $request->bio,
                                      ]);    

        }
        Session::flash('success', 'Company Profile Updated Successfully');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
