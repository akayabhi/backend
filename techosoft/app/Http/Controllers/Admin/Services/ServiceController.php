<?php

namespace App\Http\Controllers\Admin\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Services\Service;
use App\Model\Category\Primary;
use App\Model\Category\Child;
use Session;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::withTrashed()->get();
        return view('admin.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $primary_categorys = Primary::all();
        return view('admin.services.create', compact('primary_categorys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                                'primary_id'=>   'required',
                                'name'      =>   'required',
                                'content'   =>   'required',
                                'image'     =>   'required',
                           ]);

        $image = $request->image;
        $image_name  =strtotime(date('Y-m-d H:i:s')).'_'.$image->getClientOriginalName();
        $image->move('img/service', $image_name);

        $insert = Service::create([
                                'primary_id'          => $request->primary_id,
                                'parent_id'           => $request->parent_id,
                                'child_id'            => $request->child_id,
                                'meta_tag'            => $request->meta_tag,
                                'meta_description'    => $request->meta_description, 
                                'title'               => $request->name,
                                'content'             => $request->content,
                                'image'               => $image_name,
                               ]);
        Session::flash('success',$request->name. ' Service Page Added Successfully');
        return redirect()->route('service.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Service::where('id', $id)->withTrashed()->first();
        $primary_categorys = Primary::all();
        return view('admin.services.edit', compact('data','primary_categorys'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $get_page = Service::where('id',$id)->withTrashed()->first();
        if(is_null($get_page->deleted_at)){
            $delete = Service::where('id',$id)->delete();
        }
        else{
            $restore = Service::where('id', $id)->restore();
        }
        Session::flash('success', 'Service page Deleted Successfully');
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
                            'primary_id'=>   'required',
                            'name'      =>   'required',
                            'content'   =>   'required',
                            ]);

        if(isset($request->image)){
            // get image
            $check_image = Service::where('id', $id)->first();
            // image path
            $image_path = 'img/service/'.$check_image->image;
            // if image exist delte image form database
            if(file_exists($image_path)){
              unlink($image_path);
            }
            $image       = $request->image;
            $image_name  =strtotime(date('Y-m-d H:i:s')).'_'.$image->getClientOriginalName();
            $image->move('img/service', $image_name);

            $update = Service::where('id',$id)
                            ->update([
                                    'meta_tag'          => $request->meta_tag,
                                    'meta_description'  => $request->meta_description, 
                                    'primary_id'        => $request->primary_id,
                                    'parent_id'         => $request->parent_id,
                                    'child_id'          => $request->child_id,
                                    'title'             => $request->name,
                                    'content'           => $request->content,
                                    'image'             => $image_name,
                                   ]);
        }
        else{
            $update = Service::where('id',$id)
                            ->update([
                                    'meta_tag'          => $request->meta_tag,
                                    'meta_description'  => $request->meta_description, 
                                    'primary_id'        => $request->primary_id,
                                    'parent_id'         => $request->parent_id,
                                    'child_id'          => $request->child_id,
                                    'title'             => $request->name,
                                    'content'           => $request->content,
                                   ]);

        }   
        Session::flash('success',$request->name. ' Service Updated Successfully');
        return redirect()->route('service.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_child($id){

        $data = Child::where('parent_id', $id)->get()->pluck('title', 'id');
        return Response()->json($data);
    }
}
