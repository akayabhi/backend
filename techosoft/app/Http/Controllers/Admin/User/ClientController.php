<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\User\Client;
use Illuminate\Support\Facades\Hash;
use App\User;
use Session;
use DB;


class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // role_id 2 assign to clients
        $clients = User::withTrashed()->where('role_id', 2)->get();
        return view('admin.user.client.index',compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $staff = User::all();
        return view('admin.user.client.create', compact('staff'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                            'name'              =>  'required',
                            'phone'             =>  'required',
                            'email'             =>  'email',
                            'country'           =>  'required',
                            'doj'               =>  'required',
                            'password'          =>  'required|string|min:4',
                           ]);

        // client roll-Id is 2
        $insert_user = User::create([
                                    'name'          => $request->name,
                                    'email'         => $request->email,
                                    'phone'         => $request->phone,
                                    'role_id'       => '2',
                                    'referred_by'   => $request->referred_by,
                                    'password'      => Hash::make($request->password),
                                    ]);

        if(isset($request->image)){
            $image       = $request->image;
            $image_name  =strtotime(date('Y-m-d H:i:s')).'_'.$image->getClientOriginalName();
            $image->move('img/profile', $image_name);
        }
        else{
            $image_name = '';
        }

        $insert = Client::create([
                                'user_id'       => $insert_user->id,
                                'image'         => $image_name, 
                                'company_name'  => $request->company_name,
                                'profession'    => $request->profession,
                                'website'       => $request->website,
                                'country'       => $request->country,
                                'state'         => $request->state,
                                'address'       => $request->address,
                                'gst'           => $request->gst,
                                'doj'           => $request->doj,
                                'bio'           => $request->bio,
                                ]);

        Session::flash('success', 'Client Registered Successfully');
        return redirect()->route('client.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::where('id', $id)->first();
        $name = User::where('id',$data->referred_by)->first();
        $staff = User::all();
        return view('admin.user.client.edit', compact('data', 'staff', 'name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $find = User::where('id' ,$id)->withTrashed()->first();
        if(is_null($find->deleted_at)){
            $delete = User::where('id' ,$id)->delete();

            Session::flash('success',$find->name. ' Client Activated Successfully');
            return back();
        }
        else{
            $delete = User::where('id' ,$id)->restore();
            Session::flash('success',$find->name. ' Client Blocked Successfully');
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
                            'name'              =>  'required',
                            'phone'             =>  'required|min:8',
                            'email'             =>  'email',
                            'country'           =>  'required',
                            'doj'               =>  'required',
                            'referred'          =>  'required',
                           ]);
        $previous = User::where('id', $id)->first();
        // check previous email is change or not if change check this email exist in user table or not if exist you can't update any details
        if($previous->email == $request->email){
            $insert_user = User::where('id', $id)
                                  ->update([
                                            'name'          => $request->name,
                                            'phone'         => $request->phone,
                                            'referred_by'   => $request->referred,
                                           ]);

            // GET CLIENT PREVIOUS DATA 
            $client = Client::where('user_id',$id)->first();

            // IF REQUEST HAVE IMAGE THIS QUERY WILL RUN
            if(isset($request->image)){

                // UNLINK OLD IMAGE
                $image_path = 'img/profile/'.$client->image;
                 if(file_exists($image_path)){
                    if($image_path != 'img/profile/'){
                        
                        unlink($image_path);
                    }
                }

                // STORE NEW IMAGE
                $image = $request->image;
                $image_name  = strtotime(date('Y-m-d H:i:s')).'_'.$image->getClientOriginalName();
                $image->move('img/profile', $image_name);

            }
            //ELSE OLD IMAGE STORE IN THIS VARIABLE
            else{

                $image_name = $client->image;
            }

            $update_client = Client::where('user_id', $id)
                                    ->update([
                                            'image'         => $image_name, 
                                            'company_name'  => $request->company_name,
                                            'profession'    => $request->profession,
                                            'website'       => $request->website,
                                            'country'       => $request->country,
                                            'state'         => $request->state,
                                            'address'       => $request->address,
                                            'gst'           => $request->gst,
                                            'doj'           => $request->doj,
                                            'bio'           => $request->bio,
                                            ]);

            Session::flash('success', 'Client Details Updated Successfully');
            return redirect()->route('client.index');
        }   
        else{
            $find = User::where('email', $request->email)->get();
            if(count($find) == 0){
                 
                $insert_user = User::where('id', $id)->update([
                                                                'name'          => $request->name,
                                                                'email'         => $request->email,
                                                                'phone'         => $request->phone,
                                                                'referred_by'   => $request->referred,
                                                              ]);
            // GET CLIENT PREVIOUS DATA 
            $client = Client::where('user_id',$id)->first();

            // IF REQUEST HAVE IMAGE THIS QUERY WILL RUN
            if(isset($request->image)){

                // UNLINK OLD IMAGE
                $image_path = 'img/profile/'.$client->image;
                 if(file_exists($image_path)){
                    if($image_path != 'img/profile/'){

                        unlink($image_path);
                    }
                }

                // STORE NEW IMAGE
                $image = $request->image;
                $image_name  = strtotime(date('Y-m-d H:i:s')).'_'.$image->getClientOriginalName();
                $image->move('img/profile', $image_name);

            }
            //ELSE OLD IMAGE STORE IN THIS VARIABLE
            else{

                $image_name = $client->image;
            }

            $update_client = Client::where('user_id', $id)
                                    ->update([
                                            'image'         => $image_name, 
                                            'company_name'  => $request->company_name,
                                            'profession'    => $request->profession,
                                            'website'       => $request->website,
                                            'country'       => $request->country,
                                            'state'         => $request->state,
                                            'address'       => $request->address,
                                            'gst'           => $request->gst,
                                            'doj'           => $request->doj,
                                            'bio'           => $request->bio,
                                            ]);

            Session::flash('success', 'Client Details Updated Successfully');
            return redirect()->route('client.index');
            }
            else{
                Session::flash('error', 'Email Already Exist');
                return redirect()->route('staff.index');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
