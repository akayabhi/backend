<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Model\User\Staff;
use App\User;
use Session;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffs = User::where('role_id' ,3)->withTrashed()->get();
        return view('admin.user.staff.index',compact('staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.staff.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                            'image'         => 'required',
                            'name'          => 'required',
                            'dob'           => 'required',
                            'doj'           => 'required',
                            'phone'         => 'required',
                            'email'         => ['required', 'email', 'max:255', 'unique:users'],
                            'password'      =>  'required|string|min:4',
                            'bank_name'     => 'required',
                            'account_no'    => 'required',
                            'ifsc'          => 'required',
                           ]);

        // staff roll-Id is 3
        $insert_user = User::create([
                                        'name'     => $request->name,
                                        'email'    => $request->email,
                                        'phone'    => $request->phone,
                                        'role_id'  => '3',
                                        'password' => Hash::make($request->password),
                                    ]);

        $image = $request->image;
        $image_name  =strtotime(date('Y-m-d H:i:s')).'_'.$image->getClientOriginalName();
        $image->move('img/profile', $image_name);

        $insert_staff = Staff::create([
                                        'user_id'       => $insert_user->id,
                                        'image'         => $image_name,
                                        'dob'           => $request->dob,
                                        'doj'           => $request->doj,
                                        'higher_edu'    => $request->higher_edu,
                                        'skills'        => $request->skills,
                                        'second_mobile' => $request->second_mobile,
                                        'address'       => $request->address,
                                        'bank_name'     => $request->bank_name,
                                        'account_no'    => $request->account_no,
                                        'ifsc'          => $request->ifsc,
                                      ]);
        Session::flash('success', 'Employee Added Successfully');
        return redirect()->route('staff.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::where('id', $id)->first();
        return view('admin.user.staff.edit', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $find = User::where('id' ,$id)->withTrashed()->first();
        if(is_null($find->deleted_at)){
            $delete = User::where('id' ,$id)->delete();

            Session::flash('success',$find->name. ' Client Activated Successfully');
            return back();
        }
        else{
            $delete = User::where('id' ,$id)->restore();
            Session::flash('success',$find->name. ' Client Blocked Successfully');
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
                            'name'          => 'required',
                            'dob'           => 'required',
                            'doj'           => 'required',
                            'phone'         => 'required',
                            'email'         => ['email', 'max:255'],
                            'bank_name'     => 'required',
                            'account_no'    => 'required',
                            'ifsc'          => 'required',
                           ]);

        $previous = User::where('id', $id)->first();
        // check previous email is change or not if change check this email exist in user table or not if exist you can't update any details
        if($previous->email == $request->email){

            $insert_user = User::where('id', $id)->update([
                                                            'name'     => $request->name,
                                                            'phone'    => $request->phone,
                                                          ]);
            // get this employee old data 
            $employee = Staff::where('user_id',$id)->first();

            // IF REQUEST HAVE IMAGE THIS QUERY WILL RUN
            if(isset($request->image)){
                // UNLINK OLD IMAGE
                $image_path = 'img/profile/'.$employee->image;
                 if(file_exists($image_path)){
                    unlink($image_path);
                }

                // STORE NEW IMAGE
                $image = $request->image;
                $image_name  = strtotime(date('Y-m-d H:i:s')).'_'.$image->getClientOriginalName();
                $image->move('img/profile', $image_name);

            }
            //ELSE OLD IMAGE STORE IN THIS VARIABLE
            else{
                $image_name = $employee->image;
            }


            $insert_staff = Staff::where('user_id', $id)->update([
                                                            'image'         => $image_name,
                                                            'dob'           => $request->dob,
                                                            'doj'           => $request->doj,
                                                            'higher_edu'    => $request->higher_edu,
                                                            'skills'        => $request->skills,
                                                            'second_mobile' => $request->second_mobile,
                                                            'address'       => $request->address,
                                                            'bank_name'     => $request->bank_name,
                                                            'account_no'    => $request->account_no,
                                                            'ifsc'          => $request->ifsc,
                                                           ]);
            Session::flash('success', 'Employee Updated Successfully');
            return redirect()->route('staff.index');
        }   
        else{
            $find = User::where('email', $request->email)->get();
            if(count($find) == 0){
                
                $insert_user = User::where('id', $id)->update([
                                                                'name'     => $request->name,
                                                                'email'    => $request->email,
                                                                'phone'    => $request->phone,
                                                              ]);
                // get this employee old data 
                $employee = Staff::where('user_id',$id)->first();

                // IF REQUEST HAS IMAGE THIS QUERY WILL RUN
                if(isset($request->image)){
                    // UNLINK OLD IMAGE
                    $image_path = 'img/profile/'.$employee->image;
                     if(file_exists($image_path)){
                        unlink($image_path);
                    }

                    // STORE NEW IMAGE
                    $image = $request->image;
                    $image_name  = strtotime(date('Y-m-d H:i:s')).'_'.$image->getClientOriginalName();
                    $image->move('img/profile', $image_name);

                }
                //ELSE OLD IMAGE STORE IN THIS VARIABLE
                else{
                    $image_name = $employee->image;
                }


                $insert_staff = Staff::where('user_id', $id)->update([
                                                                'image'         => $image_name,
                                                                'dob'           => $request->dob,
                                                                'doj'           => $request->doj,
                                                                'higher_edu'    => $request->higher_edu,
                                                                'skills'        => $request->skills,
                                                                'second_mobile' => $request->second_mobile,
                                                                'address'       => $request->address,
                                                                'bank_name'     => $request->bank_name,
                                                                'account_no'    => $request->account_no,
                                                                'ifsc'          => $request->ifsc,
                                                               ]);
                Session::flash('success', 'Employee Updated Successfully');
                return redirect()->route('staff.index');
            }
            else{
                Session::flash('error', 'Email Already Exist');
                return redirect()->route('staff.index');
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
