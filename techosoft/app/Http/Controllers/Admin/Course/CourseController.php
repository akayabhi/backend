<?php

namespace App\Http\Controllers\Admin\Course;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function step_first(){
    	return view('admin.course.step_first');
    	
    }

    public function upload_course(){
    	return view('admin.course.upload_course');
    }
}
