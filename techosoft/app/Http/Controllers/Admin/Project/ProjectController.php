<?php

namespace App\Http\Controllers\Admin\Project;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Project\Service;
use App\Model\Project\Project;
use App\Model\Project\Order;
use App\Model\Project\ProjectServices;
use App\Model\Project\ProjectDocuments;
use App\User;
use Session; 
use DB; 

class ProjectController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::withTrashed()->get();
        return view('admin.project.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // get client from user table and fetch to create project view
        $clients = User::where('role_id', 2)->get();
        $services = Service::get();
        return view('admin.project.create', compact('clients', 'services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                            'project_name'       => 'required',
                            'client_id'          => 'required',
                            'services'           => 'required',
                            'srs'                => 'required',
                            'amount'             => 'required',
                            'referral_incentive' => 'required',
                            'timeline'           => 'required',
                            'doc'                => 'required',
        ]);

        // status: 1 Pending, 2 Working, 3 Hold, 4 Cancel, 5 Complete 
        // insert new project
        $store_project = Project::create([
                                        'client_id'    =>$request->client_id,
                                        'project_name' =>$request->project_name,
                                        'status'       =>1,
                                         ]);

        // insert in project's details order
        $store_project_details = Order::create([
                                            'project_id'          => $store_project->id,
                                            'srs'                 => $request->srs,
                                            'amount'              => $request->amount,
                                            'referral_incentive'  => $request->referral_incentive,
                                            'timeline'            => $request->timeline,
                                            ]);

        // insert order_id in Order table
        $store_order = Order::where('id', $store_project_details->id)->update([
                                    'order_id'   => 'TT000'.$store_project_details->id,
                                    ]);


        // insert project's services
        $servcies = $request->services;
        for($i=0; $i<count($servcies); $i++){
            $store_service = ProjectServices::create([
                                                    'project_id'  => $store_project->id,
                                                    'service_id'  => $servcies[$i],
                                                    ]);
        }
        // insert project's document
        foreach($request->doc as $document){
            $doc_name  =strtotime(date('Y-m-d H:i:s')).'_'.$document->getClientOriginalName();
            $document->move('img/project', $doc_name);
            $store_document  = ProjectDocuments::create([
                                                        'project_id'     => $store_project->id,
                                                        'order_id'       => $store_project_details->id,
                                                        'document_name'  => $doc_name,
                                                        ]);
        }
            
        Session::flash('success', 'Project Created Successfully');
        return redirect()->route('project.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd($id);     
        $orders   = Order::where('project_id', $id)->get();
        $details  = Order::where('project_id', $id)->first();
        // $data     = $details->project->pro_service;

        $subscribe_service  = DB::table('project_services')->join('techosoft_services','techosoft_services.id','=','project_services.service_id')
                                                 ->where('project_id',$id)
                                                 ->get();

        $selected_service  = DB::table('project_services')->join('techosoft_services','techosoft_services.id','=','project_services.service_id')
                                                 ->where('project_id',$id)
                                                 ->pluck('techosoft_services.id');
                                                
                                                 // dd($selected_service);

        $unselected_services = Service::whereNotIn('id', $selected_service)->get();


        return view('admin.project.view_project_details', compact('orders', 'subscribe_service', 'details', 'unselected_services'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orders   = Order::where('project_id', $id)->get();
        $project = Project::find($id);

        return view('admin.project.view_edit', compact('orders', 'project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
                            'srs'                => 'required',
                            'amount'             => 'required',
                            'referral_incentive' => 'required',
                            'timeline'           => 'required',
        ]);
        // $phase_data = Order::where('id', $id)->first();
        // dd($phase_data);

        $phase_data = Order::where('id', $id)->first();
        // dd($phase_data);
        if(isset($request->doc)){
            // Update in project's details
            $store_project_details = Order::where('id', $id)
                                            ->update([
                                                    'srs'                 => $request->srs,
                                                    'amount'              => $request->amount,
                                                    'referral_incentive'  => $request->referral_incentive,
                                                    'timeline'            => $request->timeline,
                                                    ]);



            // insert project's document
            foreach($request->doc as $document){
                $doc_name  =strtotime(date('Y-m-d H:i:s')).'_'.$document->getClientOriginalName();
                $document->move('img/project', $doc_name);
                $store_document  = ProjectDocuments::create([
                                                            'project_id'    => $phase_data->project_id,
                                                            'order_id'      => $phase_data->id,
                                                            'document_name' => $doc_name,
                                                            ]);
            }
            
            Session::flash('success', 'Phase Updated Successfully');
            return redirect()->route('project.edit',$phase_data->project_id);
        }
        else{
            // Update in project's details
            $store_project_details = Order::where('id', $id)
                                            ->update([
                                                    'srs'                 => $request->srs,
                                                    'amount'              => $request->amount,
                                                    'referral_incentive'  => $request->referral_incentive,
                                                    'timeline'            => $request->timeline,
                                                    ]);

            Session::flash('success', 'Phase Updated Successfully');
            return redirect()->route('project.edit',$phase_data->project_id);
            }
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*Function is use to view create new phase blade file*/
    public function add_new_phase($id)
    {
        $project = Project::find($id);
        return view('admin.project.add_new_phase', compact('project'));
    }

    /*Funciton used to store new project phase*/
    public function store_new_phase(request $request, $id){
       
        $request->validate([
                            'srs'                => 'required',
                            'amount'             => 'required',
                            'referral_incentive' => 'required',
                            'timeline'           => 'required',
                            'doc'                => 'required',
        ]);

        // insert in project's details
        $store_project_details = Order::create([
                                            'project_id'          => $id,
                                            'srs'                 => $request->srs,
                                            'amount'              => $request->amount,
                                            'referral_incentive'  => $request->referral_incentive,
                                            'timeline'            => $request->timeline,
                                            ]);

        // insert order_id in Order table
        $store_order = Order::where('id', $store_project_details->id)->update([
                                    'order_id'   => 'TT000'.$store_project_details->id,
                                    ]);

        // insert project's document
        foreach($request->doc as $document){
            $doc_name  =strtotime(date('Y-m-d H:i:s')).'_'.$document->getClientOriginalName();
            $document->move('img/project', $doc_name);
            $store_document  = ProjectDocuments::create([
                                                        'project_id'    => $id,
                                                        'order_id'      => $store_project_details->id,
                                                        'document_name' => $doc_name,
                                                        ]);
        }
        
        Session::flash('success', 'New Phase Added Successfully');
        return redirect()->route('project.index');
    }

    // function used to update status on project
    public function update_status($id, $status){
            // status: 1 Pending, 2 Working, 3 Hold, 4 Cancel, 5 Complete 
        // update status
        $update_status = Project::where('id',$id)->update(['status' => $status]);
         Session::flash('success', 'Status Updated Successfully');
        return back();
    }

    // function used to view edit_phase blade file
    public function edit_phase($id){
        $data = Order::where('id', $id)->first();
        return view('admin.project.phase_edit', compact('data'));
    }

    // function used to destory doc in phase
    public function delete_doc($id){
        $data       = ProjectDocuments::where('id',$id)->first();
        $doc_path   = 'img/project/'.$data->document_name;
        
         if (isset($doc_path)){
            if (file_exists($doc_path) == true) {
                unlink($doc_path);
            }
        
            $delte_file = ProjectDocuments::where('id',$id)->delete();
           
        }
        else{
            $delte_file = ProjectDocuments::where('id',$id)->delete();
        }
        Session::flash('success', 'Doc Deleted Successfully');
        return back();

    }

}
