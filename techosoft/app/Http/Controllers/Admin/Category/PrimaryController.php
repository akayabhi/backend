<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Category\Primary;
use Session;

class PrimaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Primary::all();
        // dd($data);
        $primary_category = Primary::withTrashed()->paginate(5);
        return view('admin.category.primary', compact('primary_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['title'=>'required']);

        $insert = Primary::create(['title'=> $request->title]);
        Session::flash('success','Primary Category Added Successfully');
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        // DELTE METHOD CALL IN THIS FUNCTION
        $get_primary_category = Primary::where('id',$id)->withTrashed()->first();

        if(is_null($get_primary_category->deleted_at)){
            $delete = Primary::where('id',$id)->delete();
        }
        else{
            $restore = Primary::where('id', $id)->restore();
        }
        return back();
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $find = Primary::find($id);
        if($find->status == 1){
            $find->status = 0;
            $find->save();
        }
        else{
            $find->status =1;
            $find->save();
        }
        Session::flash('success',$find->title.' Status Changed Successfully');
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(['title'=>'required']);

        $update = Primary::where('id',$id)->update(['title'=> $request->title]);
        Session::flash('success','Primary Category Updated Successfully');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
