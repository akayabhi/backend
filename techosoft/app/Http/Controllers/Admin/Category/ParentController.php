<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Category\Primary;
use App\Model\Category\Parent_cat;
use Session;

class ParentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Primary::withTrashed()->where('deleted_at', '!=', '')->get();
        $delete_id = array();
        foreach($data as $id){
            $delete_id[] = $id->id;
        }

        $primary_category = Primary::all();
        $parent_category = Parent_cat::withTrashed()->whereNotIn('primary_id', $delete_id)->get();
        // dd($parent_category);
        return view('admin.category.parent', compact('parent_category', 'primary_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                            'primary_id'  => 'required',
                            'title'       => 'required',
                            'image'       => 'required',
                            'description' => 'required',
                            ]);
      
        $image = $request->image;
        $image_name  =strtotime(date('Y-m-d H:i:s')).'_'.$image->getClientOriginalName();
        $image->move('img/category', $image_name);

            $insert = Parent_cat::create([
                'primary_id'     => $request->primary_id,
                'title'          => $request->title,
                'image'          => $image_name, 
                'description'    => $request->description,
        ]);
        Session::flash('success',$request->title. ' Parent Category Added Successfully');
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $primary_category = Primary::all();
        $data = Parent_cat::where('id',$id)->first();
        return view('admin.category.edit_parent', compact('data', 'primary_category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $get_Parent_cat_category = Parent_cat::where('id',$id)->withTrashed()->first();

        if(is_null($get_Parent_cat_category->deleted_at)){
            $delete = Parent_cat::where('id',$id)->delete();
        }
        else{
            $restore = Parent_cat::where('id', $id)->restore();
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());    
       $request->validate([
                        'primary_id'  => 'required',
                        'title'       => 'required',
                        'description' => 'required',
                        ]);

        if(isset($request->image)){

            $check_image = Parent_cat::where('id', $id)->first();
            $image_path = 'img/category/'.$check_image->image;
            if(file_exists($image_path)){
              unlink($image_path);
            }
            $image = $request->image;
            $image_name  =strtotime(date('Y-m-d H:i:s')).'_'.$image->getClientOriginalName();
            $image->move('img/category', $image_name);

            $update = Parent_cat::where('id', $id)->update([
                                    'primary_id'     => $request->primary_id,
                                    'title'          => $request->title,
                                    'image'          => $image_name, 
                                    'description'    => $request->description,
                                 ]);
        }
        else{
            $update = Parent_cat::where('id', $id)->update([
                                    'primary_id'     => $request->primary_id,
                                    'title'          => $request->title,
                                    'description'    => $request->description,
                                 ]);

        }

        Session::flash('success',$request->title. 'Parent Category Updated Successfully');
        return redirect()->route('parent.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
