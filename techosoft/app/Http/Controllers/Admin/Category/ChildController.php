<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Category\Primary;
use App\Model\Category\Parent_cat;
use App\Model\Category\Child;
use Session;

class ChildController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Parent_cat::withTrashed()->where('deleted_at', '!=', '')->get();
        $delete_id = array();
        foreach($data as $id){
            $delete_id[] = $id->id;
        }

        $primary_category = Primary::all();
        $child_categorys   = Child::withTrashed()->whereNotIn('parent_id', $delete_id)->get();
        return view('admin.category.child', compact('primary_category', 'child_categorys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                            'primary_id'  => 'required',
                            'parent_id'   => 'required',
                            'title'       => 'required',
                            'image'       => 'required',
                            'description' => 'required',
                            ]);
      
        $image = $request->image;
        $image_name  =strtotime(date('Y-m-d H:i:s')).'_'.$image->getClientOriginalName();
        $image->move('img/category', $image_name);

            $insert = Child::create([
                'primary_id'     => $request->primary_id,
                'parent_id'      => $request->parent_id,
                'title'          => $request->title,
                'image'          => $image_name, 
                'description'    => $request->description,
        ]);
        Session::flash('success',$request->title. ' Child Category Added Successfully');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        $primary_category = Primary::all();
        $data= Child::where('id', $id)->first();
        return view('admin.category.edit_child', compact('primary_category','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $get_Child_category = Child::where('id',$id)->withTrashed()->first();

        if(is_null($get_Child_category->deleted_at)){
            $delete = Child::where('id',$id)->delete();
        }
        else{
            $restore = Child::where('id', $id)->restore();
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
                        'primary_id'  => 'required',
                        'parent_id'  => 'required',
                        'title'       => 'required',
                        'description' => 'required',
                        ]);
       
        if(isset($request->image)){

            $check_image = Child::where('id', $id)->first();
            $image_path = 'img/category/'.$check_image->image;
            if(file_exists($image_path)){
              unlink($image_path);
            }
            $image = $request->image;
            $image_name  =strtotime(date('Y-m-d H:i:s')).'_'.$image->getClientOriginalName();
            $image->move('img/category', $image_name);
           
            $update = Child::where('id', $id)->update([
                                    'primary_id'     => $request->primary_id,
                                    'parent_id'      => $request->parent_id,
                                    'title'          => $request->title,
                                    'image'          => $image_name, 
                                    'description'    => $request->description,
                                 ]);
        }
        else{
            $update = Child::where('id', $id)->update([
                                    'primary_id'     => $request->primary_id,
                                    'parent_id'      => $request->parent_id,
                                    'title'          => $request->title,
                                    'description'    => $request->description,
                                 ]);

        }

        Session::flash('success',$request->title. ' Child Category Updated Successfully');
        return redirect()->route('child.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_parent($id){
        $data = Parent_cat::where('primary_id', $id)->get()->pluck('title', 'id');
        return Response()->json($data);
    }
}
