<?php

namespace App\Http\Controllers\Admin\Page;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Pages\Page;
use Session;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::withTrashed()->get();
        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                                'name'      =>   'required',
                                'content'   =>   'required',
                                'image'     =>   'required',
                           ]);

        $image = $request->image;
        $image_name  =strtotime(date('Y-m-d H:i:s')).'_'.$image->getClientOriginalName();
        $image->move('img/page', $image_name);

        $insert = Page::create([
                                'meta_tag'          => $request->meta_tag,
                                'meta_description'  => $request->meta_description, 
                                'title'             => $request->name,
                                'content'           => $request->content,
                                'image'             => $image_name,
                               ]);
        Session::flash('success',$request->name. ' Pages Added Successfully');
        return redirect()->route('page.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = Page::where('id', $id)->withTrashed()->first();
        return view('admin.pages.edit', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $get_page = Page::where('id',$id)->withTrashed()->first();
        if(is_null($get_page->deleted_at)){
            $delete = Page::where('id',$id)->delete();
        }
        else{
            $restore = Page::where('id', $id)->restore();
        }
        Session::flash('success', 'Page Deleted Successfully');
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
                            'name'      =>   'required',
                            'content'   =>   'required',
                       ]);

        if(isset($request->image)){

            $check_image = Page::where('id', $id)->first();
            $image_path = 'img/page/'.$check_image->image;
            if(file_exists($image_path)){
              unlink($image_path);
            }
            $image = $request->image;
            $image_name  =strtotime(date('Y-m-d H:i:s')).'_'.$image->getClientOriginalName();
            $image->move('img/page', $image_name);

            $update = Page::where('id',$id)
                            ->update([
                                    'meta_tag'          => $request->meta_tag,
                                    'meta_description'  => $request->meta_description, 
                                    'title'             => $request->name,
                                    'content'           => $request->content,
                                    'image'             => $image_name,
                                   ]);
        }
        else{
            $update = Page::where('id',$id)
                            ->update([
                                    'meta_tag'          => $request->meta_tag,
                                    'meta_description'  => $request->meta_description, 
                                    'title'             => $request->name,
                                    'content'           => $request->content,
                                   ]);

        }   
        Session::flash('success',$request->name. ' Pages Updated Successfully');
        return redirect()->route('page.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
