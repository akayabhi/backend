<?php

namespace App\Http\Controllers\Other;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use auth;

class CommonController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }

    public function darkmode()
    {

    	$find = User::find(auth::user()->id);
    	if($find->setting ==1){
    		$find->setting = 0;
    		$find->save();
    	}
    	else{
    		$find->setting = 1;
    		$find->save();
    	}
		return back();
    }
}
