<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Staff extends Model
{
	
    protected $fillable = ['user_id','image', 'dob','doj', 'higher_edu','skills', 'second_mobile', 'address', 'bank_name', 'account_no','ifsc','setting','created_at','updated_at'];
	protected $table    = 'staff';
	public $primaryKey   = 'id';
	

	/*one staff belong to one user in user table so one to one relation*/
	public function user(){
		return $this->belongsTo(User::class, 'user_id');
	}
}
