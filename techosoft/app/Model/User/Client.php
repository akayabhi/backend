<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use App\User;	

class Client extends Model
{
    
    protected $fillable = ['user_id','image', 'company_name','profession', 'website','country', 'state', 'address', 'gst', 'doj','bio','created_at','updated_at'];
	protected $table    = 'client';
	public $primaryKey   = 'id';

	public function User(){
		return $this->belongsTo(User::class ,'id','user_id');
	}
}
