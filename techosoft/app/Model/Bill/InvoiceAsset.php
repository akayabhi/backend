<?php

namespace App\Model\Bill;

use Illuminate\Database\Eloquent\Model;
use App\Model\Bill\Invoice;
use App\Model\Project\Service;

class InvoiceAsset extends Model
{
    protected $fillable    = ['invoice_id','service_id','quantity','price','created_at','updated_at'];
	protected $table       = 'invoice_assets';
	public    $primaryKey  = 'id';

	public function invoice(){
		return $this->belongsTo(Invoice::class); 
	}

	public function service(){
		return $this->belongsTo(Service::class,'service_id', 'id')->withTrashed();
	}
}
