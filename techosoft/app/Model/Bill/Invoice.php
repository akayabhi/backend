<?php

namespace App\Model\Bill;

use Illuminate\Database\Eloquent\Model;
use App\Model\Bill\InvoiceAsset;
use App\User;
use App\Model\Orders\Order;


class Invoice extends Model
{
    protected $fillable    = ['invoice_no','client_id','order_id','gst','created_at','updated_at'];
	protected $table       = 'invoices';
	public    $primaryKey  = 'id';

	public function invoice_asset(){
		return $this->hasMany(InvoiceAsset::class);
	}

	public function user(){
		return $this->belongsTo(User::class, 'client_id', 'id')->withTrashed();
	}

	public function order(){
		return $this->belongsTo(Order::class);
	}
}
