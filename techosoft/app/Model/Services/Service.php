<?php

namespace App\Model\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Category\Primary;
use App\Model\Category\Parent_cat;
use App\Model\Category\Child;

class Service extends Model
{
    use SoftDeletes;
    protected $fillable    = ['meta_tag', 'meta_description','category','primary_id','parent_id','child_id','image','title','content','created_at','updated_at','deleted_at'];
	protected $table       = 'services';
	public    $primaryKey  = 'id';

	public function primary(){
		return $this->belongsTo(Primary::class);
	}

	public function parent(){
		return $this->belongsTo(Parent_cat::class, 'parent_id', 'id');
	}

	public function child(){
		return $this->belongsTo(Child::class);
	}
}
