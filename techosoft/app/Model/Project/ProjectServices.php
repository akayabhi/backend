<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;
use App\Model\Project\Project;
use App\Model\Project\Service;

class ProjectServices extends Model
{
    protected $fillable    = ['project_id','service_id','created_at','updated_at'];
	protected $table       = 'project_services';
	public    $primaryKey  = 'id';

	

	public function project(){
		return $this->hasMany(Project::class, 'project_id', 'id'); 
	}

}
