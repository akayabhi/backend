<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;
use App\Model\Project\Order;
use App\Model\Project\ProjectServices;
use App\Model\Project\Service;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Project extends Model
{
    use SoftDeletes;
    
    protected $fillable    = ['client_id','project_name','status','project_done','created_at','updated_at','deleted_at'];
	protected $table       = 'projects';
	public    $primaryKey  = 'id';

	// many to one relationship with user table (reverse relationship)
	public function user(){
		return $this->belongsTo(User::class ,'client_id')->withTrashed();
	}

	// one to many relationship with order table 
	public function Order(){
		return $this->hasMany(Order::class);
	}

	// one to may realtionshipo with project-service
	public function pro_service(){
		return $this->hasMany(ProjectServices::class);
	}

	

}
