<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;
use App\Model\Project\Project;
use App\Model\Project\ProjectDocuments;

class Order extends Model
{
    protected $fillable    = ['project_id','srs','amount','referral_incentive','timeline','created_at','updated_at'];
	protected $table       = 'orders';
	public    $primaryKey  = 'id';

	// many to one to relationship with project (revers relationship)
	public function project(){
		return $this->belongsTo(Project::class,'project_id', 'id');
	}

	// one to many relationship with project-documents 
	public function document(){
		return $this->hasMany(ProjectDocuments::class);
	}
}
