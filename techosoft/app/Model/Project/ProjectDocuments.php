<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;
use App\Model\Project\Order;

class ProjectDocuments extends Model
{
    protected $fillable    = ['project_id', 'order_id','document_name','created_at','updated_at'];
	protected $table       = 'project_documents';
	public    $primaryKey  = 'id';

	// reverse relationship many to one
	public function order(){
		return $this->belongsTo(Order::class);
	}
}
