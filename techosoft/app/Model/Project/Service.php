<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Project\ProjectServices;
use App\Model\Bill\InvoiceAsset;

class Service extends Model
{
    use SoftDeletes;
    
    protected $fillable    = ['title','sac_code' ,'created_at','updated_at'];
	protected $table       = 'techosoft_services';
	public    $primaryKey  = 'id';

	public function project_service(){
		return $this->hasMany(ProjectServices::class, 'service_id', 'id');
	}

	public function invoice_asset(){
		return $this->hasMany(InvoiceAsset::class, 'service_id', 'id');
	}

}
