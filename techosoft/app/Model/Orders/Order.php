<?php

namespace App\Model\Orders;

use Illuminate\Database\Eloquent\Model;
use App\Model\Project\Project;
use App\Model\Bill\Invoice;

class Order extends Model
{
    protected $fillable    = ['project_id','order_no','created_at','updated_at'];
	protected $table       = 'orders';
	public    $primaryKey  = 'id';

	public function project(){
		return $this->belongsTo(Project::class);

	}

	public function invoice(){
		return $this->hasMany(Invoice::class);
	}
}
