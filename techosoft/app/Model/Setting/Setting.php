<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['company_name','logo', 'website_link','profession', 'first_email','second_email', 'first_mobile', 'second_mobile', 'address', 'gst','bank_name', 'account_no','ifsc', 'upi_account','founded', 'bio', 'created_at','updated_at'];
	protected $table    = 'setting';
	public $primaryKey   = 'id';
}
