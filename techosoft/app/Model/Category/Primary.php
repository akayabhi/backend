<?php

namespace App\Model\Category;

use Illuminate\Database\Eloquent\Model;
use App\Model\Category\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Askedio\SoftCascade\Traits\SoftCascadeTrait;


class Primary extends Model
{
	use SoftDeletes, SoftCascadeTrait;
    

	protected $fillable     = ['title', 'status', 'created_at','updated_at'];
	protected $table        = 'primary_categorys';
	public    $primaryKey   = 'id';

	protected $softCascade =['parents', 'childs'];
	protected $dates = ['deleted_at'];

	// PRIMARY CATEGORY HAS MANY PARNETS: ONE TO MANY RELATIONSHIP
	public function parents(){
		return $this->hasMany(Parent_cat::class, 'primary_id');
	}
	// PRIMARY CATEGORY HAS MANY CHILDS: ONE TO MANY RELATIONSHIP
	public function childs(){
		return $this->hasMany(Child::class, 'primary_id');
	}

}
