<?php

namespace App\Model\Category;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Askedio\SoftCascade\Traits\SoftCascadeTrait;

class Child extends Model
{
	use SoftDeletes, SoftCascadeTrait;
   
    
	protected $fillable = ['primary_id','parent_id', 'title','image', 'description', 'status', 'created_at','updated_at'];
	protected $table    = 'child_categorys';
	public $primaryKey   = 'id';

	public function primary(){
		return $this->belongsTo(Primary::class, 'primary_id');
	}

	public function parent(){
		return $this->belongsTo(Parent_cat::class, 'parent_id');
	}
}
