<?php

namespace App\Model\Category;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Askedio\SoftCascade\Traits\SoftCascadeTrait;

class Parent_cat extends Model
{
	use SoftDeletes, SoftCascadeTrait;
    

	protected $fillable = ['primary_id', 'title','image', 'description', 'status', 'created_at','updated_at'];
	protected $table    = 'parent_categorys';
	public $primaryKey   = 'id';
	protected $softCascade =['childs'];
	protected $dates = ['deleted_at'];

	// PARENT CATEGORY BELONGS TO ONE MANY: PARENTS BELONGS TO ONE PRIMARY: MANY TO ONE RELATIONSHIP
	public function primary(){

		return $this->belongsTo(Primary::class, 'primary_id');
	}
	// PARENT CATEGORY HAS MANY CHILDS ONE TO MANY RELATIONSHIP
	public function childs(){

		return $this->hasMany(Child::class, 'parent_id');
	}

}
