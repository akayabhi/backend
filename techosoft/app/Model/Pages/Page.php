<?php

namespace App\Model\Pages;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
	use SoftDeletes;
    protected $fillable    = ['meta_tag', 'meta_description','image','title','content','created_at','updated_at','deleted_at'];
	protected $table       = 'pages';
	public    $primaryKey  = 'id';
}
