<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register'=>false]);

Route::get('/home', 'HomeController@index')->name('home');

/*USER MANAGEMENT*/
// Route::resource('/clients', 'Admin\User\UserController');
Route::resource('/staff', 'Admin\User\StaffController');
Route::resource('/client', 'Admin\User\ClientController');

/*CATEGORY MANAGEMENT*/
Route::resource('/primary', 'Admin\Category\PrimaryController');
Route::resource('/parent', 'Admin\Category\ParentController');
Route::resource('/child', 'Admin\Category\ChildController');
Route::get('/child/get_parent/{id}','Admin\Category\ChildController@get_parent');
/*END CATEGORY*/

/*SITE CONFIG*/
Route::resource('/settings', 'Admin\Setting\SettingController');

/*PAGE MANAGEMENT*/
Route::resource('/page', 'Admin\Page\PageController');

/*PAGE MANAGEMENT*/
Route::resource('/service', 'Admin\Services\ServiceController');
Route::get('get/child/{id}','Admin\Services\ServiceController@get_child');

/*PROJECT MANAGEMENT*/
Route::resource('techosoft_service', 'Admin\Project\ServiceController');
Route::resource('project', 'Admin\Project\ProjectController');
Route::get('project/add_new_phase/{id}', 'Admin\Project\ProjectController@add_new_phase');
Route::post('project/store_new_phase/{id}', 'Admin\Project\ProjectController@store_new_phase');
Route::get('change/status/{id}/{status}', 'Admin\Project\ProjectController@update_status');
Route::get('edit_phase/{phase_id}', 'Admin\Project\ProjectController@edit_phase');
Route::get('delete/doc/{id}', 'Admin\Project\ProjectController@delete_doc');

/*ORDER MANAGEMENT*/
Route::resource('order', 'Admin\Order\OrderController');

/*BILL MANAGEMENT*/
Route::resource('bill', 'Admin\Bill\BillController');
Route::get('ajax/get/order_id/{id}', 'Admin\Bill\BillController@get_order');



/*Dark mode route*/
Route::get('/darkmode/status', 'Other\CommonController@darkmode')->name('darkmode.status');














// COURSE MANAGEMENT
Route::get('/step_first', 'Admin\Course\CourseController@step_first')->name('step_first');
Route::get('/upload/course', 'Admin\Course\CourseController@upload_course')->name('upload_course');










