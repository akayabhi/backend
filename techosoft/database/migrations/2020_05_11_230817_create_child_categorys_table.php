<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChildCategorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_categorys', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('primary_id');
            $table->unsignedBigInteger('parent_id');
            $table->string('title');
            $table->string('image');
            $table->string('description');
            $table->string('status')->default('1');
            $table->foreign('primary_id')->references('id')->on('primary_categorys')->onDelete('cascade');
            $table->foreign('parent_id')->references('id')->on('parent_categorys')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_categorys');
    }
}
