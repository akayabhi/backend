<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         // role_id for Admin is 1
         // role_id for Client is 2
         // role_id for Staff is 2
        
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('role_id');
            $table->string('referred_by')->nullable();
            $table->string('setting')->default('1');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
        DB::table('users')->insert([
                    'name'     => 'Techosoft Technologies',
                    'role_id'  => '1',
                    'email'    => 'admin@admin.com',
                    'password' => '$2y$12$qQTowggZyvXD5n5.qi6PJ.EAF3Eh3G/D3vzCkoF.TjsGUqW6/h32u'
                    ]);//password: admin
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
