<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('meta_tag')->nullable();
            $table->string('meta_description')->nullable();
            $table->unsignedBigInteger('primary_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->unsignedBigInteger('child_id')->nullable();
            $table->string('image');
            $table->string('title');
            $table->mediumText('content');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('primary_id')->references('id')->on('primary_categorys');
            $table->foreign('parent_id')->references('id')->on('parent_categorys');
            $table->foreign('child_id')->references('id')->on('child_categorys');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
