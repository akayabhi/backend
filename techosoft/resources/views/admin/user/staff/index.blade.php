@extends('layouts.app')
@section('title', 'User Mangement')
@section('breadcrumb')
	  <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);"> User Management</a></li>
                                  <li class="breadcrumb-item"><a href="javascript:void(0);">Staff List</a></li>
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                            <!-- Fade in down modal -->
                            <a href="{{route('staff.create')}}"  class="btn btn-outline-success mb-2"><i data-feather="user"></i><span style="margin-left: 5px;">Add Employee</span></a>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection

@section('content')
	<div class="layout-px-spacing">            
	    <div class="row layout-top-spacing">
	        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
	            <div class="widget-content widget-content-area br-6">
	                <div class="table-responsive mb-4 mt-4">
	                    <table id="alter_pagination" class="table table-hover" style="width:100%">
	                        <thead>
	                            <tr>
	                                <th>S.no</th>
	                                <th>Image</th>
	                                <th>Name</th>
	                                <th>Email</th>
	                                <th>Mobile</th>
	                                <th>Join date</th>
	                                <th class="text-center">Action</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                           	@php $sno=1; @endphp
	                          	@foreach($staffs as $data)
	                            <tr>
	                            	<td>{{$sno++}}</td>
	                                <td>
	                                   <img  src="{{URL::TO('img/profile/'.$data->staff->image)}}" style="   object-fit: scale-down;
	                                        position: relative;
	                                        height: 45px;
	                                        width: 75px;
	                                        padding: 1px;">    
	                                </td>
	                                <td>{{$data->name}}</td>
	                                <td>{{$data->email}}</td>
	                                <td>{{$data->phone}}</td>
	                                <td>{{date('d/M/Y',strtotime($data->staff->doj))}}</td>
	                                <td class="text-center">
	                                    <a href="{{route('staff.show',$data->id)}}" class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"  >  
	                                    	<i data-feather="edit" style="color: blue; margin-right:10px" ></i>
	                                    </a>
	                                    <a href="{{route('staff.edit',$data->id)}}" class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Status">
	                                    	@if(is_null($data->deleted_at))
	                                    	   <i data-feather="toggle-right" style="color: green;"></i>
	                                    	@else
	                                    	   <i data-feather="toggle-left" style="color: red;"></i>
	                                    	@endif
	                                    </a>

	                                </td>
	                            </tr>
	                            @endforeach
	                         
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

@endsection
