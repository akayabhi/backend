@extends('layouts.app')
@section('title', 'Create Clients')

@section('breadcrumb')
	  <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">User Management</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Staff</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Register Employee</a></li>
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                         <button form="add_client" type="submit" class="btn btn-outline-info btn-rounded mb-2" value="Submit">Register</button>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection

@section('content')


      <!--  BEGIN CONTENT AREA  -->
        <div class="layout-px-spacing">                
                
            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form id="add_client" action="{{route('staff.store')}}" method="post" class="section general-info" enctype="multipart/form-data">
                                  @csrf
                                  @method('POST')
                                    <div class="info">
                                        <h6 class="">General Information</h6>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-xl-2 col-lg-12 col-md-4">
                                                        <div class="upload mt-4 pr-md-4">
                                                            <input type="file" id="input-file-max-fs" class="dropify" data-default-file="assets/img/200x200.jpg" data-max-file-size="2M" / name="image" required accept="image/*">
                                                            <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> Upload Pic</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-10 col-lg-12 col-md-8 mt-md-0 mt-4">
                                                        <div class="form">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="fullName">Name <span style="color: red;">*</span></label>
                                                                        <input type="text" class="form-control mb-4" placeholder="Full Name of employee" name="name"  required="">
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="fullName">Date of Birth<span style="color: red;">*</span></label>
                                                                        <input type="text" class="form-control datepicker mb-4" placeholder="DOB of employee" name="dob"  required="" id='dob'>
                                                                    </div>
                                                                </div>

                                                           
                                                            <div class="col-sm-6">
                                                              <div class="form-group">
                                                                  <label for="profession">Date of Joining <span style="color: red;">*</span></label>
                                                                  <input type="text" class="form-control datepicker mb-4" placeholder="Joining date of employee" name="doj"  required="">
                                                              </div>
                                                            </div>

                                                           <div class="col-sm-6">
                                                              <div class="form-group">
                                                                  <label for="profession">Higher Qualifications</label>
                                                                  <input type="text" class="form-control mb-4" id="profession" placeholder="Higher Educaiton of Employee" name="higher_edu">
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12" >
                                          <div class="form-group" style="margin-top: 50px;">
                                              <label for="profession">Skills</label>
                                              <input type="text" class="form-control mb-4" id="profession" placeholder="Add Skills" name="skills">
                                          </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                             <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" style="padding: 0px 0px 25px 0px!important;" >
                                <div class="section contact">
                                    <div class="info">
                                        <h5 class="">Contact Details</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                  
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="phone">Primary Mobile No<span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control mb-4" id="phone" placeholder="Write phone number here" name="phone"  required="">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="phone">Secondary Mobile No</label>
                                                            <input type="text" class="form-control mb-4" id="phone" placeholder="Write phone number here" name="second_mobile"  >
                                                        </div>
                                                    </div>
                                                    

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="email">Email <span style="color: red;">*</span></label>
                                                            <input type="email" class="form-control mb-4" id="email" placeholder="example@gmail.com" name="email" required="">
                                                        </div>
                                                    </div>          

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address">Address</label>
                                                            <input type="text" class="form-control mb-4" placeholder="Full Address" name="address">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="text">Password <span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control mb-4" placeholder="Enter password" name="password" required="">
                                                        </div>
                                                    </div>          

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div> 
                               
                            </div>

                              <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" style="padding: 0px 0px 25px 0px!important;" >
                                <div class="section contact">
                                    <div class="info">
                                        <h5 class="">Bank Details Details</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                  
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="phone">Bank Name<span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control mb-4" placeholder="ex:State Bank of India" name="bank_name"  required="">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="phone">Account Number<span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control mb-4" placeholder="Enter Account Number" name="account_no"  required="">
                                                        </div>
                                                    </div>
                                                    

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="email">IFSC Code<span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control mb-4" placeholder="Enter Bank IFSC Code" name="ifsc" required="">
                                                        </div>
                                                    </div>            

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div> 
                                </form>
                               
                            </div>                      
                        </div>
                    </div>
                </div>
            </div>

        </div>
      <!--  END CONTENT AREA  -->


@endsection
