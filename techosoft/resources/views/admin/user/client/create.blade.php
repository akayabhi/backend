@extends('layouts.app')
@section('title', 'Create Clients')
@section('breadcrumb')
	  <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">User Management</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Client</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Register Client</a></li>
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                         <!--  <a href="#" id="create_register" class="btn btn-outline-primary btn-rounded mb-2">Register Clients</a> -->
                         <button form="add_client" type="submit" class="btn btn-outline-info btn-rounded mb-2" value="Submit">Register</button>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection

@section('content')


      <!--  BEGIN CONTENT AREA  -->
        <div class="layout-px-spacing">                
                
            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form id="add_client" action="{{route('client.store')}}" method="post" class="section general-info" enctype="multipart/form-data">
                                  @csrf
                                  @method('POST')
                                    <div class="info">
                                        <h6 class="">General Information</h6>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-xl-2 col-lg-12 col-md-4">
                                                        <div class="upload mt-4 pr-md-4">
                                                            <input type="file" id="input-file-max-fs" class="dropify" data-default-file="assets/img/200x200.jpg" data-max-file-size="2M" / name="image" accept="image/*">
                                                            <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> Upload Pic</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-10 col-lg-12 col-md-8 mt-md-0 mt-4">
                                                        <div class="form">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="fullName">Name <span style="color: red;">*</span></label>
                                                                        <input type="text" class="form-control mb-4" id="fullName" placeholder="Full Name" name="name"  required="">
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="fullName">Company Name</label>
                                                                        <input type="text" class="form-control mb-4" id="fullName" placeholder="Company Name" name="company_name"  required="">
                                                                    </div>
                                                                </div>

                                                           
                                                            <div class="col-sm-6">
                                                              <div class="form-group">
                                                                  <label for="profession">Compnay Origin</label>
                                                                  <input type="text" class="form-control mb-4" id="profession" placeholder="Origin of Comapnay" name="profession"  required="">
                                                              </div>
                                                            </div>

                                                           <div class="col-sm-6">
                                                              <div class="form-group">
                                                                  <label for="profession">Website</label>
                                                                  <input type="text" class="form-control mb-4" id="profession" placeholder="Company Website" name="website">
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" style="padding: 0px 0px 25px 0px!important;" >
                                <div class="section contact">
                                    <div class="info">
                                        <h5 class="">Contact Details</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">Country <span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control mb-4" id="
                                                            Country" placeholder="Country" value="India" name="country"  required="">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">State</label>
                                                            <input type="text" class="form-control mb-4" id="
                                                            Country" placeholder="State"  name="state"  required="">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address">Address</label>
                                                            <input type="text" class="form-control mb-4" id="address" placeholder="Full Address" name="address">
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="phone">Phone <span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control mb-4" id="phone" placeholder="Write phone number here" name="phone"  required="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="email">Email <span style="color: red;">*</span></label>
                                                            <input type="email" class="form-control mb-4" id="email" placeholder="example@gmail.com" name="email" required="">
                                                        </div>
                                                    </div>          

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="text">Password <span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control mb-4" id="email" placeholder="Enter password" name="password" required="">
                                                        </div>
                                                    </div>    

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label >Referred By</label>
                                                            <select type="text" class="form-control mb-4" id="referred_by" name="referred_by">
                                                                @foreach($staff as $marketing_staff)
                                                                <option value="{{$marketing_staff->id}}">{{$marketing_staff->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>      

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div> 
                               
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" style="padding: 0px 0px 25px 0px!important;" >
                               
                                <div class="section about">
                                    <div class="info">
                                        <h5 class="">About</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                  <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">GST No</label>
                                                            <input type="text" class="form-control mb-4" id="
                                                            Country" placeholder="GST NO" name="gst">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">Date of Join <span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control datepicker mb-4 select2" placeholder="date of join" name="doj"  required="">
                                                        </div>
                                                    </div>

                                                  </div>

                                                <div class="form-group">
                                                    <label for="aboutBio">Compnay Bio</label>
                                                    <textarea class="form-control" id="aboutBio" placeholder="Company about us" rows="5" name="bio"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 </form>
                            </div>

                           

                        </div>
                    </div>
                </div>
            </div>

        </div>
      <!--  END CONTENT AREA  -->


@endsection
