@extends('layouts.app')
@section('title', 'User Mangement')
@section('breadcrumb')
    <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Order Management</a></li>
                                  <li class="breadcrumb-item"><a href="javascript:void(0);">Order List</a></li>
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                            <!-- Fade in down modal -->
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
@endsection

@section('content')
  <div class="layout-px-spacing">            
      <div class="row layout-top-spacing">
          <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
              <div class="widget-content widget-content-area br-6">
                  <div class="table-responsive mb-4 mt-4">
                      <table id="alter_pagination" class="table table-hover" style="width:100%">
                          <thead>
                              <tr>
                                  <th>Sr no.</th>
                                  <th>Order No</th>
                                  <th>Client</th>
                                  <th>Project Title</th>
                                  <th>Amount</th>
                                  <th>Porject Status</th>
                                  <th>Order Date</th>
                                  <th>Action</th>
                                  
                              </tr>
                          </thead>
                          <tbody>
                              @php $count=1; @endphp
                                @foreach($orders as $data)
                              <tr>
                                <td>{{$count++}}</td>
                                <td>{{$data->order_id}}</td>
                                <td>{{$data->project->user->name}}</td>
                                <td>{{$data->project->project_name}}</td>
                                <td>{{$data->amount}}</td>
                                <td>
                                	@if($data->project->status == 1)
	                                		<span class="badge outline-badge-warning">Pending</span>
	                                	@elseif($data->project->status == 2)
	                                		<span class="badge outline-badge-success">Working</span>
	                                	@elseif($data->project->status == 3)
	                                		<span class="badge outline-badge-dark">Hold</span>
	                                	@elseif($data->project->status == 4)
	                                		<span class="badge outline-badge-danger">Cancel</span>
	                                	@elseif($data->project->status == 5)
	                                		<span class="badge outline-badge-info">Complete</span>
	                                	@endif
                                </td>
                                <td>{{date('h:ia d-M-y', strtotime($data->created_at))}}</td>
                                <td></td>
                              </tr>
                              @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>

@endsection
