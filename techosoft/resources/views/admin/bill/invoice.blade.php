@extends('layouts.app')
@section('title', 'Create Clients')

@section('breadcrumb')

	  <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Bill Management</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Invoice</a></li>
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                        <button form="add_client" type="submit" class="btn btn-outline-info btn-rounded mb-2" value="Submit">Print Invoice</button>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection

@section('content')

<div class="layout-px-spacing">            
      <div class="row layout-top-spacing">
          <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
              <div class="widget-content widget-content-area br-6">
                  <div class="table-responsive mb-4 mt-4">
                        <div class="doc-container">
                            <div class="invoice-00014">
                                <div class="content-section  animated animatedFadeInUp fadeInUp">

                                    <div class="row inv--head-section">

                                        <div class="col-sm-6 col-12">
                                            <h3 class="in-heading">INVOICE</h3>
                                        </div>
                                        <div class="col-sm-6 col-12 align-self-center text-sm-right">
                                            <div class="company-info">
                                                <img src="{{URL::TO('assets/img/logo_icon.png')}}" style="height: 50px;">
                                                <h5 class="inv-brand-name">TECHOSOFT TECHNOLOGIES</h5>
                                                <p class="inv-due-date"><span class="inv-title">GST : </span> <span class="inv-date">{{$tt_setting->gst}}</span></p>
                                                <p class="inv-due-date"><span class="inv-title">Email : </span> <span class="inv-date">{{$tt_setting->first_email}}</span></p>
                                            </div>
                                        </div>
                                        
                                    </div>

                                    <div class="row inv--detail-section">

                                        <div class="col-sm-7 align-self-center">
                                            <p class="inv-to">Invoice To</p>
                                        </div>
                                        <div class="col-sm-5 align-self-center  text-sm-right order-sm-0 order-1">
                                            <p class="inv-detail-title">From : Techosoft Techonologies</p>
                                        </div>
                                        
                                        <div class="col-sm-7 align-self-center">
                                            @if(isset($data->client_id))
                                                <p class="inv-customer-name">{{$data->user->name}}</p>
                                                <p class="inv-street-addr">{{$data->user->client->address}}</p>
                                                <p class="inv-email-address">{{$data->user->email}}</p>
                                            @elseif(isset($data->order_id))
                                                <p class="inv-customer-name">{{$data->order->project->user->name}}</p>
                                                <p class="inv-street-addr">{{$data->order->project->user->client->address}}</p>
                                                <p class="inv-email-address">{{$data->order->project->user->email}}</p>
                                            @endif
                                        </div>
                                        <div class="col-sm-5 align-self-center  text-sm-right order-2">
                                            <p class="inv-list-number"><span class="inv-title">Invoice Number : </span> <span class="inv-number">{{$data->invoice_no}}</span></p>
                                            <p class="inv-created-date"><span class="inv-title">Invoice Date : </span> <span class="inv-date">{{date('d-M-Y', strtotime($data->created_at))}}</span></p>
                                         <!--    <p class="inv-due-date"><span class="inv-title">Due Date : </span> <span class="inv-date">26 Aug 2019</span></p> -->
                                          
                                        </div>
                                    </div>

                                    <div class="row inv--product-table-section">
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table class="table" id="invoice_table">
                                                    <thead class="">
                                                        <tr>
                                                            <th scope="col">S.No</th>
                                                            <th scope="col">Project information</th>
                                                            <th scope="col">SAC Code</th>
                                                            <th class="text-right" scope="col">Qty</th>
                                                            <th class="text-right" scope="col">Unit Price</th>
                                                            <th class="text-right" scope="col">Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $s_no=1; $subtotal = 0; @endphp
                                                        @foreach($data->invoice_asset as $service)
                                                        <tr>
                                                            <td>{{$s_no++}}</td>
                                                            <td>{{$service->service->title}}</td>
                                                            <td>{{$service->service->sac_code}}</td>
                                                            <td class="text-right" >{{$service->quantity}}</td>
                                                            <td class="text-right price">{{$service->price}}</td>
                                                            <td class="text-right" >{{$service->quantity *$service->price}}</td>
                                                            @php $subtotal += $service->quantity *$service->price; @endphp
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-sm-5 col-12 order-sm-0 order-1">
                                            <div class="inv--payment-info">
                                                <div class="row">
                                                    <div class="col-sm-12 col-12">
                                                        <!-- <h6 class=" inv-title">Payment Info:</h6> -->
                                                    </div>
                                                    <div class="col-sm-4 col-12">
                                                        <!-- <p class=" inv-subtitle">Bank Name: </p> -->
                                                    </div>
                                                    <div class="col-sm-8 col-12">
                                                        <!-- <p class="">{{$tt_setting->bank_name}}</p> -->
                                                    </div>
                                                    <div class="col-sm-4 col-12">
                                                        <!-- <p class=" inv-subtitle">Account Number : </p> -->
                                                    </div>
                                                    <div class="col-sm-8 col-12">
                                                        <!-- <p class="">{{$tt_setting->account_no}}</p> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-7 col-12 order-sm-1 order-0">
                                            <div class="inv--total-amounts text-sm-right">
                                                <div class="row">
                                                    <div class="col-sm-8 col-7">
                                                        <p class="">Sub Total: </p>
                                                    </div>
                                                    <div class="col-sm-4 col-5">
                                                        <p class="">{{$subtotal}}</p>
                                                    </div>
                                                    <div class="col-sm-8 col-7">
                                                        <p class="">GST Amount: </p>
                                                    </div>
                                                    <div class="col-sm-4 col-5">
                                                        <p class="">{{$subtotal*$data->gst/100}}</p>
                                                    </div>
                                                    <div class="col-sm-8 col-7">
                                                        <p class=" discount-rate">Discount : <span class="discount-percentage">5%</span> </p>
                                                    </div>
                                                    <div class="col-sm-4 col-5">
                                                        <p class="">$700</p>
                                                    </div>
                                                    <div class="col-sm-8 col-7 grand-total-title">
                                                        <h4 class="">Grand Total : </h4>
                                                    </div>
                                                    <div class="col-sm-4 col-5 grand-total-amount">
                                                        <h4 class="">{{($subtotal*$data->gst/100)+($subtotal)}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                  </div>
              </div>
          </div>
      </div>
  </div>


<script type="text/javascript">
    $(document).ready(function(){

;        console.log(data);
    });
</script>
    
@endsection
