@extends('layouts.app')
@section('title', 'Create Clients')

@section('breadcrumb')

	  <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Bill Management</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Generate Bill</a></li>
                             
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                        <a href="#" class="btn btn-outline-warning btn-rounded mb-2" id="add_service" value="Submit">Add Services</a>
                        <button form="bill_form" type="submit" class="btn btn-outline-info btn-rounded mb-2" value="Submit">Submit</button>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection

@section('content')
<style type="text/css">
    .dis_none{
        display: none;" 
    }
    .
</style>

      <!--  BEGIN CONTENT AREA  -->
        <div class="layout-px-spacing">                
                
            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                        <div class="row">

                            <div class="wrap_project">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing"> 
                                <form id="bill_form" action="{{route('bill.store')}}" method="post" class="section general-info" enctype="multipart/form-data">
                                  @csrf
                                  @method('POST')
                                <div class="section contact">
                                    <div class="info">
                                        <h5 class="">Create Invoice</h5>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Create Invoice via Client or Project</label>
                                                
                                                    <div class="form-check mb-2">
                                                        <div class="custom-control custom-radio classic-radio-info">
                                                            <input type="radio" id="hRadio1" name="invoicefor" class="custom-control-input RadioInvoice" value="client" required="">
                                                            <label class="custom-control-label" for="hRadio1">Client</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-check mb-2">
                                                        <div class="custom-control custom-radio classic-radio-info">
                                                            <input type="radio" id="hRadio2" name="invoicefor" class="custom-control-input RadioInvoice" value="project">
                                                            <label class="custom-control-label" for="hRadio2">Project</label>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>

                                            <div class="col-md-6">
                                                <label>GST</label>
                                                <div class="input-group mb-4">
                                                    <input type="text" class="form-control" name="gst" placeholder="Enter GST amount">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">%</span>
                                                    </div>
                                                </div>
                                            </div>  



                                             <div class="col-md-12 dis_none" id="client_invoice" >
                                                        <div class="form-group">
                                                            <label >Client</label>
                                                            <select type="text" class="form-control mb-4" name="client_id" id="client_id">
                                                                <option value="">Select Client</option>
                                                                @foreach($clients as $client)
                                                                <option value="{{$client->id}}">{{$client->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 dis_none" id="project_invoice" > 
                                                        <div class="form-group">
                                                            <label >Project</label>
                                                            <select type="text" class="form-control mb-4" name="project_id" id="project_id">
                                                                <option value="">Select Project</option>
                                                                @foreach($projects as $project)
                                                                <option value="{{$project->id}}">{{$project->project_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 dis_none" id="order_invoice" >
                                                        <div class="form-group">
                                                            <label >Order Serial</label>
                                                            <select type="text" class="form-control mb-4" name="order_id" id="order_id">
                                                            </select>
                                                        </div>
                                                    </div>

                                            <!--add Service -->
                                            <div class="col-md-12 add_field_wrapper">
                                                <div class="row">
                                                    <div class="col-md-4"> 
                                                        <div class="form-group">
                                                            <label>Service</label>
                                                            <select type="text" class="form-control mb-4" name="service[]" required="">
                                                                <option value="">Select Service</option>
                                                                @foreach($services as $service)
                                                                <option value="{{$service->id}}">{{$service->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>   

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="text">Quantity</label>
                                                            <input type="text" class="form-control mb-4" placeholder="Quantity of Product" name="quantity[]" value="1" required="">
                                                        </div>
                                                    </div>  

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="text">Price</label>
                                                            <input type="text" class="form-control mb-4" placeholder="ex.1000" name="price[]" required="">
                                                        </div>
                                                    </div>
                                                </div>


                                                        
                                            </div>

                                            <!-- end service -->

                                        </div>
                                    </div> 
                                </div>  
                            </div>
                        </div>                      
                    </form>                               
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!--  END CONTENT AREA  -->
<!-- Add Service on bill -->

<script type="text/javascript">
$(document).ready(function(){
    var addButton = $('#add_service'); //Add button selector
    var wrapper = $('.add_field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="row"> <div class="col-md-4"> <div class="form-group"> <label>Service</label> <select type="text" class="form-control mb-4" name="service[]" required=""> <option value="">Select Service</option> @foreach($services as $service) <option value="{{$service->id}}">{{$service->title}}</option> @endforeach </select> </div> </div> <div class="col-md-4"> <div class="form-group"> <label for="text">Quantity</label> <input type="text" class="form-control mb-4" placeholder="Quantity of Product" name="quantity[]" required=""> </div> </div> <div class="col-md-2"> <div class="form-group"> <label for="text">Price</label> <input type="text" class="form-control mb-4" placeholder="Price of Product" name="price[]" required=""> </div> </div> <div class="col-md-2"> <button class="btn btn-outline-danger btn-rounded mb-2 remove_service" style="margin-top: 30px;">cancel</button> </div> </div>';

    $(addButton).click(function(){
    $(wrapper).append(fieldHTML); 

    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_service', function(e){
        e.preventDefault();
        $(this).parent().parent().remove(); //Remove field html
    });
});
</script>
<!-- end -->

@endsection
