@extends('layouts.app')
@section('title', 'Create Clients')
@section('breadcrumb')
    <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Website Setting</a>
                                </li>
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                            @if(is_null($setting))
                                <button form="add_profile" type="submit" class="btn btn-outline-success btn-rounded mb-2" value="Submit">Add Settings</button>
                            @else
                                <button form="update_profile" type="submit" class="btn btn-outline-info btn-rounded mb-2" value="Submit">Update Settings</button>
                            @endif
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection
@if(is_null($setting))
<!-- CREATE PROFILE  -->
   @section('content')
      <!--  BEGIN CONTENT AREA  -->
        <div class="layout-px-spacing">                
                
            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <!-- START FORM -->
                                <form id="add_profile" action="{{route('settings.store')}}" method="post" class="section general-info" enctype="multipart/form-data">
                                  @csrf
                                  @method('Post')
                                    <div class="info">
                                        <h6 class="">General Information</h6>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-xl-2 col-lg-12 col-md-4">
                                                        <div class="upload mt-4 pr-md-4">
                                                            <input type="file" id="input-file-max-fs" class="dropify" data-default-file=""  data-max-file-size="2M" / name="logo" accept="image/*">
                                                            <p class="mt-2" style="text-align: center;"><i class="flaticon-cloud-upload mr-1"></i> Upload Logo</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-10 col-lg-12 col-md-8 mt-md-0 mt-4">
                                                        <div class="form">
                                                            <div class="row">

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="fullName">Company Name <span></span></label>
                                                                        <input type="text" class="form-control mb-4" id="fullName" name="company_name" placeholder="Enter Your Comapany Name">
                                                                    </div>
                                                                </div>

                                                           
                                                            <div class="col-sm-6">
                                                              <div class="form-group">
                                                                  <label for="profession">Profession <span></span></label>
                                                                  <input type="text" class="form-control mb-4" id="profession" name="profession" placeholder="Enter Your Company Profession"> 
                                                              </div>
                                                            </div>

                                                            <div class="col-sm-6">
                                                              <div class="form-group">
                                                                  <label for="profession">Website Link</label>
                                                                  <input type="text" class="form-control mb-4" name="website_link" placeholder="Enter Your Website Link">
                                                              </div>
                                                            </div>

                                                          </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- CONTACT DETAILS -->
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" style="padding: 0px 0px 25px 0px!important;" >
                                <div class="section contact">
                                    <div class="info">
                                        <h5 class="">Contact Details</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">First Email<span></span></label>
                                                            <input type="email" class="form-control mb-4" id="
                                                            Country" name="first_email" placeholder="Enter Your Email">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">Second Email<span></span></label>
                                                            <input type="email" class="form-control mb-4" id="
                                                            other_email" name="second_email" placeholder="Enter Your Other Email">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address">Primary Number</label>
                                                            <input type="text" class="form-control mb-4" id="first_mobile" name="first_mobile" placeholder="Enter Your Primary Phone no">
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="phone">Second Number<span></span></label>
                                                            <input type="text" class="form-control mb-4" id="second_mobile" name="second_mobile"  placeholder="Enter Your Secondary Phone no">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="email">Address<span></span></label>
                                                            <input type="addresss" class="form-control mb-4" id="address" name="address"  placeholder="Enter Your Full Address">
                                                        </div>
                                                    </div>          
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div> 
                            </div>

                            <!-- BANK DETAILS -->
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" style="padding: 0px 0px 25px 0px!important;" >
                                <div class="section contact">
                                    <div class="info">
                                        <h5 class="">Bank Details</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                    
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">Bank Name<span></span></label>
                                                            <input type="text" class="form-control mb-4" id="
                                                            bank_name" name="bank_name"  placeholder="Enter Your Bank Name">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address">Account Number</label>
                                                            <input type="text" class="form-control mb-4" id="account_no" name="account_no" placeholder="Enter Your Account Number">
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>ifsc <span></span></label>
                                                            <input type="text" class="form-control mb-4" id="ifsc" name="ifsc" placeholder="Enter Your IFSC">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="email">UPI Account <span></span></label>
                                                            <input type="text" class="form-control mb-4" id="upi_account" name="upi_account" placeholder="Enter Your UPI Account address">
                                                        </div>
                                                    </div>   

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">GST<span></span></label>
                                                            <input type="text" class="form-control mb-4" name="gst" placeholder="Enter Your GST Number">
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div> 
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" style="padding: 0px 0px 25px 0px!important;" >
                               
                                <div class="section about">
                                    <div class="info">
                                        <h5 class="">About</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                  <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">Foundation Date<span></span></label>
                                                            <input type="text" class="form-control mb-4 select2"  name="founded" placeholder="Company Foundation Date">
                                                        </div>
                                                    </div>

                                                  </div>

                                                <div class="form-group">
                                                    <label for="aboutBio">Compnay Bio</label>
                                                    <textarea class="form-control" id="bio" placeholder="Tell us about Company" rows="7" name="bio"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM -->
                            </div>

                           

                        </div>
                    </div>
                </div>
            </div>

        </div>
      <!--  END CONTENT AREA  -->


    @endsection
@else
    <!-- UPDATE PROFILE -->
     @section('content')
      <!--  BEGIN CONTENT AREA  -->
        <div class="layout-px-spacing">                
                
            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <!-- START FORM -->
                                <form id="update_profile" action="{{route('settings.update',$setting->id)}}" method="post" class="section general-info" enctype="multipart/form-data">
                                  @csrf
                                  @method('PUT')
                                    <div class="info">
                                        <h6 class="">General Information</h6>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-xl-2 col-lg-12 col-md-4">
                                                        <div class="upload mt-4 pr-md-4">
                                                            <input type="file" id="input-file-max-fs" class="dropify" data-default-file="{{URL::TO('img/setting/'.$setting->logo)}}"  data-max-file-size="2M" / name="logo" >
                                                            <p class="mt-2" style="text-align: center;"><i class="flaticon-cloud-upload mr-1"></i> Upload Logo</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-10 col-lg-12 col-md-8 mt-md-0 mt-4">
                                                        <div class="form">
                                                            <div class="row">

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="fullName">Company Name <span></span></label>
                                                                        <input type="text" class="form-control mb-4" id="fullName" name="company_name" placeholder="Enter Your Comapany Name" value="{{$setting->company_name}}">
                                                                    </div>
                                                                </div>

                                                           
                                                            <div class="col-sm-6">
                                                              <div class="form-group">
                                                                  <label for="profession">Profession <span></span></label>
                                                                  <input type="text" class="form-control mb-4" id="profession" name="profession" placeholder="Enter Your Company Profession" value="{{$setting->profession}}"> 
                                                              </div>
                                                            </div>

                                                            <div class="col-sm-6">
                                                              <div class="form-group">
                                                                  <label for="profession">Website Link</label>
                                                                  <input type="text" class="form-control mb-4" name="website_link" placeholder="Enter Your Website Link" value="{{$setting->website_link}}">
                                                              </div>
                                                            </div>

                                                          </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- CONTACT DETAILS -->
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" style="padding: 0px 0px 25px 0px!important;" >
                                <div class="section contact">
                                    <div class="info">
                                        <h5 class="">Contact Details</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">First Email<span></span></label>
                                                            <input type="email" class="form-control mb-4" id="
                                                            Country" name="first_email" placeholder="Enter Your Email" value="{{$setting->first_email}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">Second Email<span></span></label>
                                                            <input type="email" class="form-control mb-4" id="
                                                            other_email" name="second_email" placeholder="Enter Your Other Email" value="{{$setting->second_email}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address">Primary Number</label>
                                                            <input type="text" class="form-control mb-4" id="first_mobile" name="first_mobile" placeholder="Enter Your Primary Phone no" value="{{$setting->first_mobile}}">
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="phone">Second Number<span></span></label>
                                                            <input type="text" class="form-control mb-4" id="second_mobile" name="second_mobile"  placeholder="Enter Your Secondary Phone no" value="{{$setting->second_mobile}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="email">Address<span></span></label>
                                                            <input type="addresss" class="form-control mb-4" id="address" name="address" placeholder="Enter Your Full Address" value="{{$setting->addresss}}">
                                                        </div>
                                                    </div>          
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div> 
                            </div>

                            <!-- BANK DETAILS -->
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" style="padding: 0px 0px 25px 0px!important;" >
                                <div class="section contact">
                                    <div class="info">
                                        <h5 class="">Bank Details</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                    
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">Bank Name<span></span></label>
                                                            <input type="text" class="form-control mb-4" id="
                                                            bank_name" name="bank_name"  placeholder="Enter Your Bank Name" value="{{$setting->bank_name}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address">Account Number</label>
                                                            <input type="text" class="form-control mb-4" id="account_no" name="account_no" placeholder="Enter Your Account Number" value="{{$setting->account_no}}">
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>ifsc <span></span></label>
                                                            <input type="text" class="form-control mb-4" id="ifsc" name="ifsc" placeholder="Enter Your IFSC" value="{{$setting->ifsc}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="email">UPI Account <span></span></label>
                                                            <input type="text" class="form-control mb-4" id="upi_account" name="upi_account" placeholder="Enter Your UPI Account address"  value="{{$setting->upi_account}}">
                                                        </div>
                                                    </div>   

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">GST<span></span></label>
                                                            <input type="text" class="form-control mb-4" name="gst" placeholder="Enter Your GST Number" value="{{$setting->gst}}" >
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div> 
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" style="padding: 0px 0px 25px 0px!important;" >
                               
                                <div class="section about">
                                    <div class="info">
                                        <h5 class="">About</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                  <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">Foundation Date<span></span></label>
                                                            <input type="text" class="form-control mb-4 select2"  name="founded" placeholder="Company Foundation Date" value="{{$setting->founded}}" >
                                                        </div>
                                                    </div>

                                                  </div>

                                                <div class="form-group">
                                                    <label for="aboutBio">Compnay Bio</label>
                                                    <textarea class="form-control" id="bio" placeholder="Tell us about Company" rows="7" name="bio">{{html_entity_decode($setting->bio)}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM -->
                            </div>

                           

                        </div>
                    </div>
                </div>
            </div>

        </div>
      <!--  END CONTENT AREA  -->


    @endsection
@endif

