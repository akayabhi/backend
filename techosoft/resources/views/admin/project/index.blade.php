@extends('layouts.app')
@section('title', 'Project Management')
@section('breadcrumb')
	  <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);"> User Management</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Project Lists</a></li>
                            </ol>
                        </nav>
                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                           	<a href="{{route('project.create')}}"  class="btn btn-outline-success btn-rounded mb-2"><i data-feather="user"></i><span style="margin-left: 5px;">Add Project</span></a>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection

@section('content')
	<div class="layout-px-spacing">            
	    <div class="row layout-top-spacing">
	        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
	            <div class="widget-content widget-content-area br-6">
	                <div class="table-responsive mb-4 mt-4">
	                    <table id="alter_pagination" class="table table-hover" style="width:100%">
	                        <thead>
	                            <tr>
	                                <th>S.no</th>
	                                <th>Client</th>
	                                <th>Project</th>
	                                <th>Services</th>
	                                <th>Phase</th>
	                                <th>Amount</th>
	                                <th>Status</th>
	                                <th class="text-center">Action</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                           	@php $sno=1; @endphp
	                          	@foreach($projects as $data)
	                            <tr>
	                            	<td>{{$sno++}}</td>
	                                <td>{{$data->user->name}}</td>
	                                <td>{{$data->project_name}}</td>
	                                <td>{{service($data->id)}}</td>
	                              	<td>{{count($data->order)}}</td>
	                                <td>
	                                	@php $total_amount = 0;@endphp
	                                	@foreach($data->order as $amount)
	                                		@php $total_amount += $amount->amount @endphp
	                                	@endforeach
	                                	{{$total_amount}}.00
	                                	
	                                </td>

	                                <td>
	                                	@if($data->status == 1)
	                                		<span class="badge outline-badge-warning">Pending</span>
	                                	@elseif($data->status == 2)
	                                		<span class="badge outline-badge-success">Working</span>
	                                	@elseif($data->status == 3)
	                                		<span class="badge outline-badge-dark">Hold</span>
	                                	@elseif($data->status == 4)
	                                		<span class="badge outline-badge-danger">Cancel</span>
	                                	@elseif($data->status == 5)
	                                		<span class="badge outline-badge-info">Complete</span>
	                                	@endif
	                                </td>
	                             
	                                <td class="text-center">

	                                	<!-- Add new Phase -->
	                                	<a href="{{URL::TO('project/add_new_phase/'.$data->id)}}" class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="New Phase"  >  
	                                    	<i data-feather="file-plus" style="color: blue; margin-right:10px" ></i>
	                                    </a>

	                                    <!-- Edit Project Details -->
	                                    <a href="{{route('project.edit',$data->id)}}" class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i data-feather="edit" style="color: blue; margin-right:10px" ></i>
	                                    </a>

	                                    <!-- View  Project Details-->
	                                    <a href="{{route('project.show',$data->id)}}" class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="View Project Details"><i data-feather="eye" style="color: blue; margin-right:10px" ></i>
	                                    </a>

	                                    <!-- Change Status  -->
	 									<a class="dropdown-toggle" href="#" role="button" id="status" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	 									<i data-feather="chevrons-down" style="color: blue; margin-right:10px" ></i>
	 									</a>
	                                    <div class="dropdown-menu" aria-labelledby="status">
	                                        <a class="dropdown-item" href="{{URL::TO('change/status/'.$data->id .'/'.'1')}}">Pending</a>
	                                        <a class="dropdown-item" href="{{URL::TO('change/status/'.$data->id .'/'.'2')}}">Working</a>
	                                        <a class="dropdown-item" href="{{URL::TO('change/status/'.$data->id .'/'.'3')}}">Hold</a>
	                                        <a class="dropdown-item" href="{{URL::TO('change/status/'.$data->id .'/'.'4')}}">Cancel</a>
	                                        <a class="dropdown-item" href="{{URL::TO('change/status/'.$data->id .'/'.'5')}}">Complete</a>
	                                    </div>
	                                </td>
	                            </tr>
	                            @endforeach
	                         
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

@php
    function service($id){
        $subscribe_service  = DB::table('project_services')
                                    ->join('techosoft_services','techosoft_services.id','=','project_services.service_id')
                                    ->where('project_id',$id)
                                    ->pluck('title');
	    return $subscribe_service->implode(' , ');
	}
@endphp

@endsection
