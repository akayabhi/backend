@extends('layouts.app')
@section('title', 'Create Clients')

@section('breadcrumb')

	  <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Project Management</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);"><span class="badge badge-info">Project Name: {{$data->project->project_name}}</span></a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Edit Phase</a></li>
                               
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                        <button form="edit_phase" type="submit" class="btn btn-outline-info btn-rounded mb-2" value="Submit">Edit phase</button>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection

@section('content')
<style type="text/css">
    .doc{
        height: 100px;
        width: 100px;
        position: relative;
        object-fit: contain;
        border: 1px solid gray;
        margin-right: 10px;
    }
</style>


      <!--  BEGIN CONTENT AREA  -->
        <div class="layout-px-spacing">                
                
            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                        <div class="row">

                            <div class="wrap_project">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" style="padding: 0px 0px 25px 0px!important;" >
                                <form id="edit_phase" action="{{route('project.update',$data->id)}}" method="post" class="section general-info" enctype="multipart/form-data">
                                  @csrf
                                  @method('PUT')
                                <div class="section contact">
                                    <div class="info">
                                        <h5 class="">Phase Details</h5>

                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address">Phase Cost</label>
                                                            <input type="text" class="form-control mb-4" placeholder="Project Amount" name="amount" value="{{$data->amount}}" required="" >
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label >Referral Incentive</label>
                                                            <input type="text" class="form-control mb-4" placeholder="ex.4000" name="referral_incentive" value="{{$data->referral_incentive}}" required="">
                                                        </div>
                                                    </div>          
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="text">Timeline</label>
                                                            <input type="text" class="form-control mb-4" placeholder="Enter Days to Complete the project" name="timeline" value="{{$data->timeline}}" required="">
                                                        </div>
                                                    </div>  

                                                     <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Message<span style="color: red;">*</span></label>
                                                            <textarea type="text" class="form-control mb-4" placeholder="write the Software Requirement and Specification" name="srs"  required="" rows="6">{{$data->srs}}</textarea>
                                                        </div>
                                                    </div> 

                                                     <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Previous Uploaded Document</label>
                                                            <br>
                                                            @foreach($data->document as $doc)
                                                                <img alt="Document" src="{{URL::TO('img/project/'.$doc->document_name)}}" class=" bs-popover rounded doc" data-container="body" data-trigger="hover" data-content="{{$doc->document_name}}">
                                                              
                                                                    <a href="{{URL::TO('delete/doc/'.$doc->id)}}">
                                                                        <i data-feather="x-square" style="color: red;margin-top:-60px" ></i>  
                                                                    </a>
                                                             
                                                            @endforeach
                                                        </div>
                                                    </div> 


                                                     <div class="col-md-12">
                                                        <div class="widget-content widget-content-area">
                                                            <div class="custom-file-container" data-upload-id="mySecondImage">
                                                                <label>Upload Document <small style="color: red">(Allow Multiple)</small> <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                                                <label class="custom-file-container__custom-file" >
                                                                    <input type="file" class="custom-file-container__custom-file__custom-file-input" multiple name="doc[]">
                                                                    <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                                                                    <span class="custom-file-container__custom-file__custom-file-control"></span>
                                                                </label>
                                                                <div class="custom-file-container__image-preview"></div>
                                                            </div>
                                                        </div>              

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>  
                            </div>
                        </div>                      
                    </form>                               
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!--  END CONTENT AREA  -->

@endsection
