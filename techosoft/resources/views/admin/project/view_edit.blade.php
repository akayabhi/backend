@extends('layouts.app')
@section('title', 'Project')

@section('breadcrumb')

	  <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Project Management</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);"><span class="badge badge-info">Project: {{$project->project_name}}</span></a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Phase List</a></li>
                                <li class="breadcrumb-item " ><a href="javascript:void(0);"><span class="badge badge-info">Client: {{$project->user->name}}</span></a>
                                </li>
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                      <!--   <button form="add_client" type="submit" class="btn btn-outline-info btn-rounded mb-2" value="Submit">Edit Project</button> -->
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection



@section('content')
    <div class="layout-px-spacing">            
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <div class="table-responsive mb-4 mt-4">
                        <table id="alter_pagination" class="table table-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>S.no</th>
                                    <th>Project Name</th>
                                    <th>Amount</th>
                                    <th>Referral Incentive</th>
                                    <th>Timeline</th>
                                    <th>Docs</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $sno=1; @endphp
                                 @foreach($orders as $phase)
                                <tr>
                                    <td>{{$sno++}}</td>
                                    <td>{{$phase->project->project_name}}</td>
                                    <td>{{$phase->amount}}</td>
                                    <td>{{$phase->referral_incentive}}</td>
                                    <td>{{$phase->timeline}}</td>
                                    <td>{{count($phase->document)}}</td>
                                    <td class="text-center">

                                        <!-- edit Phase -->
                                        <a href="{{URL::TO('edit_phase/'.$phase->id)}}" class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Phase"  >  
                                        <i data-feather="edit" style="color: blue; margin-right:15px" ></i>
                                        </a>

                                    </td>
                                </tr>
                                @endforeach
                             
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
