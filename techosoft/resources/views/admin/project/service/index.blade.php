@extends('layouts.app')
@section('title', 'User Mangement')
@section('breadcrumb')

   
	  <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);"> Project Management</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Service</a></li>
                            </ol>
                        </nav>
                    </div>
                </li>  
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection

@section('content')

<div class="main-content">
   <div class="col-12">
      <div class="container">
         <div class="row">
            <div id="flStackForm" class="col-md-4 layout-spacing layout-top-spacing">
               <div class="statbox widget box box-shadow">
                  <div class="widget-header">
                     <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                           <h4>Add Service</h4>
                        </div>
                     </div>
                  </div>
                  <div class="widget-content widget-content-area">
                     <form method="post" action="{{route('techosoft_service.store')}}" method="post">
                        @csrf
                        @method('POST')
                        <div class="form-group mb-3">
                       <!--    <label>Service Title</label> -->
                          <input type="text" class="form-control" name="title"  placeholder="Service Name">
                          <br>
                        <!--   <label>SAC Code</label> -->
                          <input type="text" class="form-control" name="sac_code"  placeholder="SAC Code">
                        </div>
                        <button type="submit" class="btn btn-primary mt-3">Submit</button>
                     </form>
                  </div>
               </div>
            </div>

            <div id="flStackForm" class="col-md-8 layout-spacing layout-top-spacing">
               <div class="statbox widget box box-shadow">
                  <div class="widget-header">
                     <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                           <h4>Primary Categories List</h4>
                        </div>
                     </div>
                  </div>
                  <div class="widget-content widget-content-area">
                     <table id="zero-config" class="table table-hover" style="width:100%">
                        <thead>
                           <tr>
                              <th>Sr no.</th>
                              <th>Service</th>
                              <th>Sac Code</th>
                              <th>Status</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                                @php $count=1; @endphp
                                @foreach($services as $data)
                            <tr>
                                <td>{{$count++}}</td>
                                <td>{{$data->title}}</td>
                                 <td>{{$data->sac_code}}</td>
                                <td>
                                    @if(is_null($data->deleted_at))
                                       <span><strong>Active</strong></span>
                                    @else
                                       <span style="color: red;"><strong>Inactive</strong></span>
                                    @endif
                                </td>
                                <td>
                                 <a  data-toggle="modal" data-target="#primary{{$data->id}}"><i data-feather="edit" style="color: blue; margin-right:5px;"></i></a>

                                <!-- Soft Delete -->
                                <a class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="{{route('techosoft_service.show',$data->id)}}" data-confirm="Are you sure you want to delete this?">
                                    @if(is_null($data->deleted_at))
                                       <i data-feather="toggle-right" style="color: green;"></i>
                                    @else
                                       <i data-feather="toggle-left" style="color: red;"></i>
                                    @endif

                                  </a>


                                </td>

                              <!-- Modal -->
                              <div class="modal fade" id="primary{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                 <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                       <form method="post" action="{{route('techosoft_service.update',$data->id)}}">
                                          <div class="modal-header">
                                             <h5 class="modal-title" id="exampleModalLabel">Edit Service</h5>
                                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             </button>
                                          </div>
                                          <div class="modal-body">
                                             @csrf
                                             @method('put')
                                             <div class="form-group mb-3">
                                                <input type="text" class="form-control" name="title" value="{{$data->title}}">
                                                <br>
                                                <input type="text" class="form-control" name="sac_code"  placeholder="SAC Code" value="{{$data->sac_code}}">

                                             </div>
                                          </div>
                                          <div class="modal-footer">
                                             <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                                             <button type="submit" class="btn btn-primary">Update</button>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </tr>
                          @endforeach
                        </tbody>
                     </table>
                     <div style="position: relative;margin-left: 40%;margin-right: 50%;">
                        {{ $services->links() }}
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
   
@endsection
