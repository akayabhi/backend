@extends('layouts.app')
@section('title', 'Create Clients')

@section('breadcrumb')

	  <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Project Management</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Create new project</a></li>
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                        <button form="add_client" type="submit" class="btn btn-outline-info btn-rounded mb-2" value="Submit">Create Project</button>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection

@section('content')


      <!--  BEGIN CONTENT AREA  -->
        <div class="layout-px-spacing">                
                
            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form id="add_client" action="{{route('project.store')}}" method="post" class="section general-info" enctype="multipart/form-data">
                                  @csrf
                                  @method('POST')
                                    <div class="info">
                                        <h5 class="">General Details</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                  
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label >Project Name<span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control mb-4" id="name" placeholder="Write project name here" name="project_name"  required="">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label >Client</label>
                                                            <select type="text" class="form-control mb-4" name="client_id">
                                                            	<option>--Select client--</option>
                                                            	@foreach($clients as $client)
                                                            	<option value="{{$client->id}}">{{$client->name}}</option>
                                                            	@endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label >Services</label>
                                                            <select type="text" class="form-control" id="project_services"  name="services[]" multiple="multiple">
	                                                        	@foreach($services as $service)
	                                                        	<option value="{{$service->id}}">{{$service->title}}</option>
	                                                        	@endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="wrap_project">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" style="padding: 0px 0px 25px 0px!important;" >
                                <div class="section contact">
                                    <div class="info">
                                        <h5 class="">Project Details</h5>

                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Message<span style="color: red;">*</span></label>
                                                            <textarea type="text" class="form-control mb-4" placeholder="write the Software Requirement and Specification" name="srs"  required="" rows="5"></textarea>
                                                        </div>
                                                    </div>                                                   
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address">Project Cost</label>
                                                            <input type="text" class="form-control mb-4" placeholder="Project Amount" name="amount">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label >Referral Incentive</label>
                                                            <input type="text" class="form-control mb-4" placeholder="ex.4000" name="referral_incentive" required="">
                                                        </div>
                                                    </div>          
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="text">Timeline</label>
                                                            <input type="text" class="form-control mb-4" placeholder="Enter Days to Complete the project" name="timeline" required="">
                                                        </div>
                                                    </div>   

                                                    <div class="col-md-12">
                                                        <div class="widget-content widget-content-area">
                                                            <div class="custom-file-container" data-upload-id="mySecondImage">
                                                                <label>Upload (Allow Multiple) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                                                <label class="custom-file-container__custom-file" >
                                                                    <input type="file" class="custom-file-container__custom-file__custom-file-input" multiple name="doc[]">
                                                                    <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                                                                    <span class="custom-file-container__custom-file__custom-file-control"></span>
                                                                </label>
                                                                <div class="custom-file-container__image-preview"></div>
                                                            </div>
                                                        </div>       
                                                        
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                          
                            </div>                      
                        </form>                               
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!--  END CONTENT AREA  -->
    
@endsection
