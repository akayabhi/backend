@extends('layouts.app')
@section('title', 'User Mangement')
@section('breadcrumb')
    <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Category Management</a></li>
                                  <li class="breadcrumb-item"><a href="javascript:void(0);">Parent Category</a></li>
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                            <!-- Fade in down modal -->
                          <a  data-toggle="modal" data-target="#create_primary_model"><span  class="btn btn-outline-primary mb-2">Add Parent Category</span></a>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->

    <!-- BEGIN ADD PRIMARY CATEGORY MODEL -->
    <div class="modal fade" id="create_primary_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog" role="document">
          <div class="modal-content">
             <form  action="{{route('parent.store')}}" method="post" enctype="multipart/form-data">
             @csrf
             @method('post')
                <div class="modal-header">
                   <h5 class="modal-title" id="exampleModalLabel">Add Parent Category</h5>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   </button>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                      <label>Primary Category</label>
                      <select type="text" class="form-control" name="primary_id" aria-describedby="emailHelp1" placeholder="Primary Category" required>
                          <option value="">Select Primary Category</option>
                          @foreach($primary_category as $row)
                             <option value="{{$row->id}}">{{$row->title}}</option>
                          @endforeach
                      </select>
                    </div>

                    <div class="form-group mb-3">
                      <label>Parent Category</label>
                        <input type="text" class="form-control" name="title" aria-describedby="emailHelp1" placeholder="Title" required="">
                    </div>

                    <div class="form-group mb-3">
                        <label>Description</label>
                        <textarea type="text" class="form-control" name="description"  aria-describedby="emailHelp1" rows="5" placeholder="Write Desctiption" required=""></textarea>
                    </div>

                   <div class=" form-group  mb-3 col-xl-6 col-lg-12 col-md-4" style="margin-left: -15px!important;">
                        <div class="upload mt-4 pr-md-4">
                          <label>Upload Picture</label>
                            <input type="file" id="input-file-max-fs" class="dropify" data-default-file="" data-max-file-size="2M" / name="image" required="" accept="image/*">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                   <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                   <button type="submit" class="btn btn-primary">Update</button>
                </div>
             </form>
          </div>
       </div>
    </div>
    <!-- END MODEL -->

@endsection

@section('content')
  <div class="layout-px-spacing">            
      <div class="row layout-top-spacing">
          <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
              <div class="widget-content widget-content-area br-6">
                  <div class="table-responsive mb-4 mt-4">
                      <table id="alter_pagination" class="table table-hover" style="width:100%">
                          <thead>
                              <tr>
                                  <th>Sr no.</th>
                                  <th>Image</th>
                                  <th>Primary Category</th>
                                  <th>Parent Title</th>
                                  <th>Status</th>
                                  <th>Action</th>
                                  
                              </tr>
                          </thead>
                          <tbody>
                              @php $count=1; @endphp
                                @foreach($parent_category as $data)
                              <tr>
                                <td>{{$count++}}</td>
                                <td>
                                  <img  src="{{URL::TO('img/category/'. $data->image)}}" style="    object-fit: scale-down;
                                        position: relative;
                                        height: 54px;
                                        width: 75px;
                                        padding: 1px;">    
                                </td>
                                <td>{{$data->primary->title}}</td>
                                <td>{{$data->title}}</td>
                                <td>
                                    @if(is_null($data->deleted_at))
                                       <span><strong>Active</strong></span>
                                    @else
                                       <span style="color: red;"><strong>Inactive</strong></span>
                                    @endif
                                </td>
                                <td>
                                  <a href="{{route('parent.show',$data->id)}}"><i data-feather="edit" style="color: blue; margin-right:5px;"  ></i></a>

                                  <a class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Status" href="{{route('parent.edit',$data->id)}}">
                                    @if(is_null($data->deleted_at))
                                       <i data-feather="toggle-right" style="color: green;"></i>
                                    @else
                                       <i data-feather="toggle-left" style="color: red;"></i>
                                    @endif
                                  </a>

                                </td>
                              </tr>
                              @endforeach
                           
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>

@endsection
