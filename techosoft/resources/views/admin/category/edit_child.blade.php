@extends('layouts.app')
@section('title', 'Create Clients')
@section('breadcrumb')
	  <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Category Management</a></li>
                                  <li class="breadcrumb-item"><a href="javascript:void(0);">Edit Child Category</a></li>
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                         <!--  <a href="#" id="create_register" class="btn btn-outline-primary btn-rounded mb-2">Register Clients</a> -->
                         <!-- <button form="add_client" type="submit" class="btn btn-outline-primary btn-rounded mb-2" value="Submit">Update client</button> -->
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection

@section('content')


      <!--  BEGIN CONTENT AREA  -->
        <div class="layout-px-spacing">                
                
            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                 <form  action="{{route('child.update', $data->id)}}" class="section general-info"  method="post" enctype="multipart/form-data">
                                       @csrf
                                       @method('PUT')
                                          <div class="modal-header">
                                             <h5 class="modal-title">Edit Child Category</h5>
                                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             </button>
                                          </div>
                                          <div class="modal-body">
                                              <div class="form-group mb-3">
                                                <label>Primary Category</label>
                                                <select type="text" class="form-control" id="primary_id" name="primary_id" aria-describedby="emailHelp1" placeholder="Primary Category" required>
                                                    <option value="{{$data->primary_id}}">{{$data->primary->title}}</option>
                                                    <option value="{{$data->primary_id}}">--Select Primary Category Below--</option>
                                                    @foreach($primary_category as $row)
                                                       <option value="{{$row->id}}">{{$row->title}}</option>
                                                    @endforeach
                                                </select>
                                              </div>

                                                <div class="form-group mb-3">
                                                  <label>Parent Category</label>
                                                  <select type="text" class="form-control" name="parent_id" id="parent_id" aria-describedby="emailHelp1"required>
                                                    <option value="{{$data->parent_id}}">{{$data->parent->title}}</option>
                                                    <option value="{{$data->parent_id}}">--Select Parent Category--</option>
                                                  </select>
                                                </div>


                                              <div class="form-group mb-3">
                                                <label>Child Category</label>
                                                  <input type="text" class="form-control" name="title" aria-describedby="emailHelp1" placeholder="Title" value="{{$data->title}}" required="">
                                              </div>

                                              <div class="form-group mb-3">
                                                  <label>Description</label>
                                                  <textarea type="text" class="form-control" name="description" aria-describedby="emailHelp1" rows="5" placeholder="Write Desctiption">{{html_entity_decode($data->description)}}</textarea>
                                              </div>

                                              <div class=" form-group  mb-3 col-xl-6 col-lg-12 col-md-4" style="margin-left: -15px!important;">
                                                  <div class="upload mt-4 pr-md-4">
                                                    <label>Upload Picture</label>
                                                      <input type="file" id="input-file-max-fs" class="dropify" data-default-file="{{URL::TO('img/category'.'/'.$data->image)}}" data-max-file-size="2M" / name="image" accept="image/*">
                                                  </div>
                                              </div>

                                          </div>
                                          <div class="modal-footer">
                                             <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                                             <button type="submit" class="btn btn-primary">Update</button>
                                          </div>
                                       </form>
                            </div>                                   
                        </div>
                    </div>
                </div>
            </div>

        </div>
      <!--  END CONTENT AREA  -->


@endsection
