@extends('layouts.app')
@section('title', 'Create Clients')
@section('breadcrumb')
	  <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Create Course</a></li>
                                 <li class="breadcrumb-item"><a href="javascript:void(0);">Upload Video</a></li>
                                  
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                          <!-- right side button -->
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection


@section('content')
    
    <!--  BEGIN CONTENT AREA  -->
        <div class="layout-px-spacing">                
                
            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example field_wrapper" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                        <!-- content body -->
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form id="work-platforms" class="section work-platforms">
                                    <div class="info">
                                        <h4>Start Putting together your course by creating Lectures, and learning notes</h4>
                                        
                                        <div class="row">
                                            
                                            <div class="col-md-11 mx-auto">

                                                <div class="platform-div">
                                                    <div class="form-group">
                                                        <label for="platform-title">Lecture Title</label>
                                                        <input type="text" class="form-control mb-4" id="platform-title" placeholder="Ex: Introduction" value=""  >
                                                        
                                                        <div class="form-group">
                                                        <label for="platform-description">Description</label>
                                                        <textarea class="form-control mb-4" id="platform-description" placeholder="Platforms Description" rows="10">Add Description for Course</textarea>
                                                    	</div>
                                                        <label for="platform-title">Video</label>
                                                        <input type="file" class="form-control mb-4" id="platform-title" placeholder="Upload Video" value="">

                                                        <label for="platform-title">Resource</label>
                                                        <input type="file" class="form-control mb-4" id="platform-title" placeholder="Ex:pdf etc" value="">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- end content body -->

                    </div>
                </div>
            </div>
        </div>
      <div>
      	  <a href="#" id="add_button" class="add_button btn btn-primary" style="margin-top: -10px;
    margin-left: 15px;">  <i data-feather="plus"></i> Add Lecture</a>
      </div>


     <!--  END CONTENT AREA  -->
  

@endsection

