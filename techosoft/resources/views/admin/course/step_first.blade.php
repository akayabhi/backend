@extends('layouts.app')
@section('title', 'Create Clients')
@section('breadcrumb')
	  <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Create Course</a></li>
                                 <li class="breadcrumb-item"><a href="javascript:void(0);">Target Your Students</a></li>
                                  
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                          <!-- right side button -->
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection


@section('content')
    
    <!--  BEGIN CONTENT AREA  -->
        <div class="layout-px-spacing">                
                
            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form id="work-platforms" class="section work-platforms">
                                    <div class="info">
                                        <h5 class="">Target Your Students</h5>
                                        <div class="row">
                                            
                                            <div class="col-md-11 mx-auto">

                                                <div class="platform-div">
                                                    <div class="form-group">
                                                        <label for="platform-title">What will Student Learn form the Course?</label>
                                                        <input type="text" class="form-control mb-4" id="platform-title" placeholder="Ex: They will learn basics of C and C++ form this course" value=""  >

                                                    </div>

                                                    <div class="form-group">
                                                        <label for="platform-title">Are there any course requirements or prerequisites?</label>
                                                        <input type="text" class="form-control mb-4" id="platform-title" placeholder="Ex: Basic knowledge of C and C++" value=""  >

                                                    </div>

                                                    <div class="form-group">
                                                        <label for="platform-title">Who are your target student?</label>
                                                        <input type="text" class="form-control mb-4" id="platform-title" placeholder="Ex: Students of Under graduate" value=""  >

                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <!--  END CONTENT AREA  -->
  

@endsection
