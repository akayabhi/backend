@extends('layouts.app')
@section('title', 'User Mangement')
@section('breadcrumb')
    <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="javascript:void(0);">CMS Management</a></li>
                              <li class="breadcrumb-item"><a href="javascript:void(0);">Page List</a></li>
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                          <a  href="{{route('page.create')}}"><span  class="btn btn-outline-success mb-2">Add New Pages</span></a>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->

@endsection

@section('content')
  <div class="layout-px-spacing">            
      <div class="row layout-top-spacing">
          <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
              <div class="widget-content widget-content-area br-6">
                  <div class="table-responsive mb-4 mt-4">
                      <table id="alter_pagination" class="table table-hover" style="width:100%">
                          <thead>
                              <tr>
                                  <th>Sr no.</th>
                                  <th>Image</th>
                                  <th>Title</th>
                                  <th>Content</th>
                                  <th>Status</th>
                                  <th>Action</th>
                                  
                              </tr>
                          </thead>
                          <tbody>
                              @php $count=1; @endphp
                                @foreach($pages as $data)
                              <tr>
                                <td>{{$count++}}</td>
                                <td>
                                   <img  src="{{URL::TO('img/page/'. $data->image)}}" style="   object-fit: scale-down;
                                        position: relative;
                                        height: 54px;
                                        width: 75px;
                                        padding: 1px;">    
                                </td>
                                <td>{{$data->title}}</td>
                                <td>{{substr($data->content,0,60)}}</td>
                                <td>
                                    @if(is_null($data->deleted_at))
                                       <span><strong>Active</strong></span>
                                    @else
                                       <span style="color: red;"><strong>Inactive</strong></span>
                                    @endif
                                </td>
                                <td>
                                  <a href="{{route('page.show',$data->id)}}"><i data-feather="edit" style="color: blue; margin-right:5px;"  ></i></a>

                                  <a class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Status" href="{{route('page.edit',$data->id)}}">
                                    @if(is_null($data->deleted_at))
                                       <i data-feather="toggle-right" style="color: green;"></i>
                                    @else
                                       <i data-feather="toggle-left" style="color: red;"></i>
                                    @endif
                                  </a>
                                </td>
                              </tr>
                              @endforeach
                           
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>

@endsection
