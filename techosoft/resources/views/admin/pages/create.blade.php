@extends('layouts.app')
@section('title', 'CMS')
@section('breadcrumb')
	  <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);"> CMS Management</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);"> Other Page</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Create New Page</a></li>
                            </ol>
                        </nav>


                    </div>
                </li>  
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <div class="col-lg-12">
                             <button form="add_page" type="submit" class="btn btn-outline-primary btn-rounded mb-2" value="Submit">Add Page</button>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
    

@endsection

@section('content')


      <!--  BEGIN CONTENT AREA  -->
        <div class="layout-px-spacing">                
                
            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form id="add_page" action="{{route('page.store')}}" method="post" class="section general-info" enctype="multipart/form-data">
                                  @csrf
                                  @method('POST')
                                    <div class="info">
                                        <h6 class="">Page Information</h6>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-xl-2 col-lg-12 col-md-4">
                                                        <div class="upload mt-4 pr-md-4">
                                                            <input type="file" id="input-file-max-fs" class="dropify" data-default-file="assets/img/200x200.jpg" data-max-file-size="2M" / name="image" required accept="image/*">
                                                           <small style="text-align: center;">Upload Image</small>
                                                        </div>
                                                    </div>
                                                   
                                                </div> 
                                                <div class="row" style="margin-top: 20px;"> 

                                                     <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="fullName">Meta Tag</label>
                                                            <input type="text" class="form-control mb-4" id="fullName" placeholder="Enter Meta Tag" name="meta_tag"  required="">
                                                        </div>
                                                    </div>
                                                     <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="fullName">Meta Description</label>
                                                            <input type="text" class="form-control mb-4" id="fullName" placeholder="Enter Meta Description" name="meta_description"  required="">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label for="fullName">Page Name<span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control mb-4" id="fullName" placeholder="Page Name" name="name"  required="">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label for="aboutBio">Page Content<span style="color: red;">*</span></label>
                                                            <textarea class="form-control" id="aboutBio" placeholder="Write Content for Page" rows="25" name="content" required=""></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
      <!--  END CONTENT AREA  -->


@endsection
