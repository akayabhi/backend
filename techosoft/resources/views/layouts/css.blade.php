@if(Auth::user()->setting != 0)
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <script src="{{URL::To('assets/js/libs/jquery-3.1.1.min.js')}}"></script>
    <link rel="icon" type="image/x-icon" href="{{URL::To('assets/img/logo_icon.png')}}"/>
 
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">
    <link href="{{URL::To('bootstrap/jquery_ui/jquery-ui.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::To('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::To('assets/css/plugins.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::TO('assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="{{URL::To('plugins/apex/apexcharts.css')}}" rel="stylesheet" type="text/css">
    <link href="{{URL::To('assets/css/dashboard/dash_2.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->

    <!-- DATA TABLE-->
    <link rel="stylesheet" type="text/css" href="{{URL::To('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::To('plugins/table/datatable/dt-global_style.css')}}">

    <!--  BEGIN  FORM CUSTOM STYLE FILE  -->
    <link rel="stylesheet" type="text/css" href="{{URL::To('plugins/dropify/dropify.min.css')}}">
    <link href="{{URL::To('assets/css/users/account-setting.css')}}" rel="stylesheet" type="text/css" />
    <!--  END FORM CUSTOM STYLE FILE  -->

     <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="{{URL::To('assets/css/components/cards/card.css')}}" rel="stylesheet" type="text/css" />
    <!--  END CUSTOM STYLE FILE  -->    

    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link rel="stylesheet" type="text/css" href="{{URL::TO('plugins/select2/select2.min.css')}}">
    
    <!--  END CUSTOM STYLE FILE  -->
    <link href="{{URL::TO('plugins/notification/snackbar/snackbar.min.css')}}" rel="stylesheet" type="text/css" />

    <!-- Awesome Font CDN -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Element Dropdown -->
    <link href="{{URL::TO('assets/css/elements/miscellaneous.css')}}" rel="stylesheet" type="text/css"/>

    <!-- form file upload -->
    <link href="{{URL::TO('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css" />
    
    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="{{URL::TO('assets/css/apps/invoice.css')}}" rel="stylesheet" type="text/css" />
    

@else
    <!-- DARK MODE CSS -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <script src="{{URL::To('darkmode/assets/js/libs/jquery-3.1.1.min.js')}}"></script>
    <link rel="icon" type="image/x-icon" href="{{URL::To('assets/img/logo_icon.png')}}"/>
    <link href="{{URL::To('darkmode/assets/css/loader.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{URL::To('darkmode/assets/js/loader.js')}}"></script>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">
    <link href="{{URL::To('bootstrap/jquery_ui/jquery-ui.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::To('darkmode/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::To('darkmode/assets/css/plugins.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <!-- <link href="{{URL::To('darkmode/plugins/apex/apexcharts.css')}}" rel="stylesheet" type="text/css"> -->
    <link href="{{URL::To('darkmode/assets/css/dashboard/dash_2.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->

    <!-- DATA TABLE-->
    <link rel="stylesheet" type="text/css" href="{{URL::To('darkmode/plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::To('darkmode/plugins/table/datatable/dt-global_style.css')}}">
    <!-- END -->

    <!--  BEGIN  FORM CUSTOM STYLE FILE  -->
    <link rel="stylesheet" type="text/css" href="{{URL::To('darkmode/plugins/dropify/dropify.min.css')}}">
    <link href="{{URL::To('darkmode/assets/css/users/account-setting.css')}}" rel="stylesheet" type="text/css" />
    <!--  END FORM CUSTOM STYLE FILE  -->
@endif