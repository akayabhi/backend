@if(auth::user()->setting !=0)
     <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="{{URL::To('assets/js/libs/jquery-3.1.1.min.js')}}"></script>
    <script src="{{URL::To('bootstrap/js/popper.min.js')}}"></script>
    <script src="{{URL::To('bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::To('plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{URL::To('plugins/font-icons/feather/feather.min.js')}}"></script>
    <script src="{{URL::To('assets/js/app.js')}}"></script>
    <!-- -------------------------------------------------------------------------------------- -->
    <script src="{{URL::To('bootstrap/jquery_ui/jquery-ui.min.js')}}"></script>
    <script src="{{URL::TO('plugins/notification/snackbar/snackbar.min.js')}}"></script>
    <script src="{{URL::TO('assets/js/scrollspyNav.js')}}"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="{{URL::To('assets/js/custom.js')}}"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
 
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    <script src="{{URL::To('plugins/apex/apexcharts.min.js')}}"></script>
    <script src="{{URL::To('assets/js/dashboard/dash_2.js')}}"></script>
  

    <!-- feather icon  -->
    <script type="text/javascript">
        feather.replace();
    </script>
   
    <!--  INPUT FORM SCRIPT  --> 
    <script src="{{URL::To('plugins/dropify/dropify.min.js')}}"></script>
    <script src="{{URL::To('plugins/blockui/jquery.blockUI.min.js')}}"></script>
    <!-- <script src="plugins/tagInput/tags-input.js"></script> -->
    <script src="{{URL::To('assets/js/users/account-settings.js')}}"></script>
    <!--  END FORM  -->


    <!-- START DATATABLE SCRIPT-->
    <script src="{{URL::To('plugins/table/datatable/datatables.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#alter_pagination').DataTable( {
                "pagingType": "full_numbers",
                "oLanguage": {
                    "oPaginate": { 
                        "sFirst": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg>',
                        "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                        "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
                        "sLast": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>'
                    },
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                   "sLengthMenu": "Results :  _MENU_",
                },
                "stripeClasses": [],
                "lengthMenu": [7, 10, 20, 50],
                "pageLength": 7 
            });
        } );
    </script>
  

     <!--  BEGIN CARD SCRIPT -->
    <script src="{{URL::TO('plugins/highlight/highlight.pack.js')}}"></script>
    <!--  END CARD SCRIPT  -->

     <!-- DATE PICK IN ADMIN/USER MANAGEMENT/CREATE AND EDIT -->
      <script>
      $(function () {
        //Date picker
        $('.datepicker').datepicker({
          autoclose: true
          
        })
      })
    </script>
    <!-- END -->

    <!--  BEGIN CUSTOM SCRIPTS FILE  -->
    <script src="{{URL::TO('plugins/select2/select2.min.js')}}"></script>
    <script src="{{URL::TO('plugins/select2/custom-select2.js')}}"></script>
    <!--  BEGIN CUSTOM SCRIPTS FILE  -->
    
    <!-- Notification script -->
    <script src="{{URL::TO('assets/js/components/notification/custom-snackbar.js')}}"></script>

    <!-- multiple file upload -->
    <script src="{{URL::TO('plugins/file-upload/file-upload-with-preview.min.js')}}"></script>

    <script>

        //Second upload
        var secondUpload = new FileUploadWithPreview('mySecondImage')
    </script>

    <!-- Invoice script -->
    <script src="{{URL::To('assets/js/apps/invoice.js')}}"></script>
   

@else
    <!-- DARK MODE JS -->
     <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="{{URL::To('darkmode/assets/js/libs/jquery-3.1.1.min.js')}}"></script>
    <script src="{{URL::To('darkmode/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{URL::To('darkmode/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::To('darkmode/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>

    <script src="{{URL::To('darkmode/plugins/font-icons/feather/feather.min.js')}}"></script>
    <script src="{{URL::To('darkmode/assets/js/app.js')}}"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="{{URL::To('darkmode/assets/js/custom.js')}}"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

      <!-- extra add -->
     <script src="{{URL::To('bootstrap/jquery_ui/jquery-ui.min.js')}}"></script>
    <!-- extra add -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    <!-- <script src="{{URL::To('darkmode/plugins/apex/apexcharts.min.js')}}"></script> -->
    <script src="{{URL::To('darkmode/assets/js/dashboard/dash_2.js')}}"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->

    <!-- FONT ICON SCRIPT  -->
    <script type="text/javascript">
        feather.replace();
    </script>
    <!-- END FONT ICON SCRIPT -->

    <!--  INPUT FORM SCRIPT  --> 

    <script src="{{URL::To('darkmode/plugins/dropify/dropify.min.js')}}"></script>
    <script src="{{URL::To('darkmode/plugins/blockui/jquery.blockUI.min.js')}}"></script>
    <!-- <script src="plugins/tagInput/tags-input.js"></script> -->
    <script src="{{URL::To('darkmode/assets/js/users/account-settings.js')}}"></script>
    <!--  END FORM  -->


    <!-- START DATATABLE SCRIPT-->
    <script src="{{URL::To('darkmode/plugins/table/datatable/datatables.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#alter_pagination').DataTable( {
                "pagingType": "full_numbers",
                "oLanguage": {
                    "oPaginate": { 
                        "sFirst": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg>',
                        "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                        "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
                        "sLast": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>'
                    },
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                   "sLengthMenu": "Results :  _MENU_",
                },
                "stripeClasses": [],
                "lengthMenu": [7, 10, 20, 50],
                "pageLength": 7 
            });
        } );
    </script>
    <!-- END DATA TABLE SCRIPT -->

    <!-- DATE PICK IN ADMIN/USER MANAGEMENT/CREATE AND EDIT -->
      <script>
      $(function () {
        //Date picker
        $('.datepicker').datepicker({
            year: ture
          autoclose: true
          
        })
      })
    </script>
    <!-- END -->

@endif