 <!--  BEGIN SIDEBAR  -->
        <div class="sidebar-wrapper sidebar-theme">
            
            <nav id="sidebar">
                <div class="shadow-bottom"></div>
                <ul class="list-unstyled menu-categories" id="accordionExample">

                     <!-- BEGIN DASHBOARD -->
                    <li class="menu">
                        <a href="{{route('home')}}" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-cpu"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                            <span>Dashboard</span>
                            </div>
                        </a>
                    </li>    
                    <!-- END -->

                    <!-- BEGIN USER MANAGEMENT -->
                    <li class="menu">
                        <a href="#user" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                 <i data-feather="users"></i>
                                <span>User</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="user" data-parent="#accordionExample">
                            <li>
                                <a href="{{route('client.index')}}">Client</a>
                            </li>

                             <li>
                                <a href="{{route('staff.index')}}">Staff</a>
                            </li>
                           
                        </ul>
                    </li>
                    <!-- END User MANAGEMENT -->


                    <!-- BEGIN CATEGORY MANAGEMENT -->
                    <li class="menu">
                        <a href="#Category" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                 <i data-feather="layers"></i>
                                <span>Category</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="Category" data-parent="#accordionExample">
                            <li>
                                <a href="{{route('primary.index')}}">Primary Category</a>
                            </li>

                             <li>
                                <a href="{{route('parent.index')}}">Parent Category</a>
                            </li>

                             <li>
                                <a href="{{route('child.index')}}">Child Category</a>
                            </li>
                           
                           
                        </ul>
                    </li>
                    <!-- END CATEGORY MANAGEMENT -->

                    <!-- BEGIN PAGE SETTING -->
                    <li class="menu">
                        <a href="#cms" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                 <i data-feather="book-open"></i>
                                <span>CMS</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="cms" data-parent="#accordionExample">
                            <li>
                                <a href="{{route('page.index')}}">Other Pages</a>
                            </li>

                             <li>
                                <a href="{{route('service.index')}}">Services Page</a>
                            </li>                          
                           
                        </ul>
                    </li>
                    <!-- END CATEGORY MANAGEMENT -->

                    <!-- BEGIN PROJECT MANAGEMENT -->
                    <li class="menu">
                        <a href="#project" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                 <i data-feather="gitlab"></i>
                                <span>Project</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="project" data-parent="#accordionExample">
                            <li>
                                <a href="{{route('techosoft_service.index')}}">Services</a>
                            </li>

                            <li>
                                <a href="{{route('project.index')}}">Project</a>
                            </li>                          
                           
                        </ul>
                    </li>
                    <!-- END CATEGORY MANAGEMENT -->

                    <!-- BEGIN PROJECT MANAGEMENT -->
                    <li class="menu">
                        <a href="#order" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                 <i data-feather="shopping-cart"></i>
                                <span>Order</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="order" data-parent="#accordionExample">
                            <li>
                                <a href="{{route('order.index')}}">Order</a>
                            </li>
                        </ul>
                    </li>
                    <!-- END -->


                    <!-- BEGIN BILL MANAGEMENT -->
                    <li class="menu">
                        <a href="#bill" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                <i data-feather="file"></i>
                                <i data-feather="dollar-sign" style="margin-left: -35px;height: 14px;"></i>
                                <span>Bill</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="bill" data-parent="#accordionExample">
                            <li>
                                <a href="{{route('bill.index')}}">Invoice</a>
                            </li>
                        </ul>
                    </li>
                    <!-- END CATEGORY MANAGEMENT -->

                    <!-- BEGIN SETTING -->
                    <li class="menu">
                        <a href="{{route('settings.index')}}" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                <i data-feather="settings"></i>
                                <span>Setting</span>
                            </div>
                        </a>
                    </li>
                    <!-- END -->
                    
                </ul>
            </nav>
        </div>
        <!--  END SIDEBAR  -->