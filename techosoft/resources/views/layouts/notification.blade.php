<script>
    @if(count($errors) > 0)
        @foreach($errors->all() as $error)
            Snackbar.show({
                text: '{{ $error }}',
                actionTextColor: '#fff',
                backgroundColor: '#e7515a',
                pos: 'bottom-right'
            });
        @endforeach
    @endif
</script>
<script>
  @if(Session::has('success'))
  		Snackbar.show({
                text: '{{  Session::get('success')  }}',
                actionTextColor: '#fff',
                backgroundColor: '#8dbf42',
                pos: 'bottom-right'
            });
  @endif
  @if(Session::has('info'))
  		Snackbar.show({
                text: '{{  Session::get('info')  }}',
                actionTextColor: '#fff',
                backgroundColor: '#2196f3',
                pos: 'bottom-right'
            });
  @endif
  @if(Session::has('warning'))
  		Snackbar.show({
                text: '{{  Session::get('warning')  }}',
                actionTextColor: '#fff',
                backgroundColor: '#e2a03f',
                pos: 'bottom-right'
            });
  @endif
  @if(Session::has('error'))
    Snackbar.show({
          text: '{{  Session::get('dander')  }}',
          actionTextColor: '#fff',
          backgroundColor: '#e7515a'
      });
  		toastr.error("{{ Session::get('error') }}");
  @endif
</script>