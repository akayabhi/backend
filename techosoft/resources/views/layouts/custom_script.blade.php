
<!-- AJAX USED TO GET PARENT CATEGORY WHEN CHANGE IN PRIAMRY CATEGORY-->
<script type="text/javascript">
    $(document).ready(function(){
        $('#primary_id').change(function(){
            var primary_id = $("#primary_id").val();
            $.ajax({
                type:"GET",
                url:"{{URL::To('child/get_parent')}}/"+primary_id,
                success:function(result){
                    if(result){
                        $("#parent_id").empty();
                        $("#child_id").empty();
                        $("#parent_id").append('<option value="">--Select Parent Category--</option>');
                        $("#child_id").append('<option value="">--Select Child Category--</option>');
                        $.each(result,function(key,value){
                          $("#parent_id").append('<option value="'+key+'">'+value+'</option>');
                       });
                    }
                }
            });
        });
    });

</script>
<!-- END -->

<!-- AJAX USED TO GET CHILD CATEGORY WHEN CHANGE IN PARENT CATEGORY-->
<script type="text/javascript">
    $(document).ready(function(){
        $('#parent_id').change(function(){
            var parent_id = $("#parent_id").val();
            $.ajax({
                type:"GET",
                url:"{{URL::To('get/child')}}/"+parent_id,
                success:function(result){
                    if(result){
                        $("#child_id").empty();
                        $("#child_id").append('<option value="">--Select Parent Category--</option>');
                        $.each(result,function(key,value){
                          $("#child_id").append('<option value="'+key+'">'+value+'</option>');
                       });
                    }
                }
            });
        });
    });

</script>
<!-- END -->

<!-- PROJECT SERVICE-->
<script type="text/javascript">
    $("#project_services").select2({
        tags: true
    });
</script>
<!-- END -->

<!--Create Invoice page-->
<script type="text/javascript">
    $(document).ready(function(){
        $('.RadioInvoice').click(function(){
        var data = $("input[name='invoicefor']:checked").val();
        if(data == 'client'){
            $('#client_invoice').removeClass('dis_none');
            $('#project_invoice').addClass('dis_none');
            $('#order_invoice').addClass('dis_none');
        }
        else if(data == 'project'){
            $('#client_invoice').addClass('dis_none');
            $('#project_invoice').removeClass('dis_none');
            $('#order_invoice').removeClass('dis_none');
            $("#order_id").append('<option value="">--Select Project First--</option>');
        }
        // console.log(data);
    });
    });
</script>

<!-- AJAX USED TO GET ORDER ID WHEN CHANGE IN PROJECT-->
<script type="text/javascript">
    $(document).ready(function(){
        $('#project_id').change(function(){
            var project_id = $("#project_id").val();
            $.ajax({
                type:"GET",
                url:"{{URL::To('ajax/get/order_id')}}/"+project_id,
                success:function(result){
                    if(result){
                        $("#order_id").empty();
                        $("#order_id").append('<option value="">--Select Order Id--</option>');
                        $.each(result,function(key,value){
                          $("#order_id").append('<option value="'+key+'">'+value+'</option>');
                       });
                    }
                }
            });
        });
    });

</script>
<!-- END -->




